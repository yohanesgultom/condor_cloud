-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 11, 2017 at 09:43 
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `condor_cloud`
--

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE `application` (
  `id` int(11) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `command_argument`
--

CREATE TABLE `command_argument` (
  `id` int(11) NOT NULL,
  `command_main_id` int(11) NOT NULL,
  `string` varchar(500) DEFAULT NULL,
  `argument_type` varchar(500) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `content_sample` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `command_main`
--

CREATE TABLE `command_main` (
  `id` int(11) NOT NULL,
  `pool_id` int(11) NOT NULL,
  `apps_id` int(11) NOT NULL,
  `string` varchar(500) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `experiment`
--

CREATE TABLE `experiment` (
  `id` int(11) NOT NULL,
  `user_id` varchar(500) NOT NULL,
  `pool_id` int(11) NOT NULL,
  `apps_id` int(11) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `folder_location` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE `job` (
  `id` int(11) NOT NULL,
  `experiment_id` int(11) NOT NULL,
  `condor_id` varchar(500),
  `string` varchar(500) DEFAULT NULL,
  `starting_time` datetime DEFAULT NULL,
  `finished_time` datetime DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `status` varchar(500) DEFAULT NULL,
  `execution` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `machine`
--

CREATE TABLE `machine` (
  `id` int(11) NOT NULL,
  `pool_id` int(11) NOT NULL,
  `ip_address` varchar(500) DEFAULT NULL,
  `hostname` varchar(500) DEFAULT NULL,
  `root_username` varchar(500) DEFAULT NULL,
  `root_password` varchar(500) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `local_path` varchar(500) NOT NULL,
  `nfs_path` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pool`
--

CREATE TABLE `pool` (
  `id` int(11) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `pool_type` varchar(500) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `folder_path` varchar(500) NOT NULL,
  `headNode_ip` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(500) NOT NULL,
  `password` varchar(500) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `role` varchar(500) DEFAULT NULL,
  `profile_picture` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table data
--

LOCK TABLES `users` WRITE;
INSERT INTO `users` (`username`, `password`, `name`, `role`, `profile_picture`, `email`) VALUES
('admin', '81dc9bdb52d04dc20036dbd8313ed055', 'admin', 'admin', NULL, 'admin@admin.com'); -- 1234
UNLOCK TABLES;

LOCK TABLES `application` WRITE;
/*!40000 ALTER TABLE `application` DISABLE KEYS */;
INSERT INTO `application` VALUES (1,'Gromacs','Molecular dynamics package mainly designed for simulations of proteins, lipids, and nucleic acids'),(2,'Amber','Molecular dynamics simulation application  used in a variety of simulation programs'),(3,'CUDA','A parallel computing platform and programming model created by NVIDIA'),(4,'MPI','Standard MPI Library');
/*!40000 ALTER TABLE `application` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `pool` WRITE;
/*!40000 ALTER TABLE `pool` DISABLE KEYS */;
INSERT INTO `pool` VALUES (2,'AWS Pool','Cluster','','/home/ubuntu/experiment','172.31.15.97');
/*!40000 ALTER TABLE `pool` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `machine` WRITE;
/*!40000 ALTER TABLE `machine` DISABLE KEYS */;
INSERT INTO `machine` VALUES (2,2,'172.31.15.97','ip-172-31-15-97','ubuntu','ippiVIL5nU74','Head Node','','/home/ubuntu/experiment', '/home/ubuntu/open-share');
/*!40000 ALTER TABLE `machine` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `command_main` WRITE;
/*!40000 ALTER TABLE `command_main` DISABLE KEYS */;
INSERT INTO `command_main` VALUES (3,2,1,'/usr/local/gromacs/bin/gmx','Standard Gromacs'),(5,2,1,'/usr/bin/mpirun','Running MPI'),(6,2,3,'./','Running a Program'),(7,2,4,'/usr/bin/mpirun','Runnning MPI Application'),(8,2,4,'/usr/bin/mpicc','');
/*!40000 ALTER TABLE `command_main` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `command_argument` WRITE;
/*!40000 ALTER TABLE `command_argument` DISABLE KEYS */;
INSERT INTO `command_argument` VALUES (7,3,'editconf','OTHERS','Config Simualtion Box','-'),(8,3,'-f','INPUT','input','conf.gro'),(9,3,'-bt','OTHERS','-bt','-'),(10,3,'dodecahedron','OTHERS','dodecahedron','-'),(11,3,'-o','OUTPUT','output','box.gro'),(12,3,'-d','OTHERS','-d','-'),(13,3,'0.5','OTHERS','size','-'),(14,3,'solvate','OTHERS','Solvating water','-'),(15,3,'-cp','INPUT','-cp','box.gro'),(17,3,'-p','INPUT','-p','topol.top'),(18,3,'mdrun','OTHERS','mdrun','-'),(19,3,'-v','OTHERS','-v','-'),(20,3,'-deffnm','OTHERS','-deffnm','-'),(21,3,'pr','OTHERS','pr','-'),(22,3,'-cs','OTHERS','-cs','-'),(23,3,'spc216.gro','OTHERS','spc216.gro','-'),(24,5,'-np','OTHERS','Number of process','16'),(25,5,'16','OTHERS','Process','-'),(26,5,'/usr/local/gromacs/bin/gmx_mpi','OTHERS','gmx',''),(27,5,'-f','INPUT','Hostfile','mpi_hostfile'),(28,5,'mdrun','OTHERS','mdrun','-'),(29,5,'-v','OTHERS','-v','-'),(30,5,'-deffnm','OTHERS','-deffnm','-'),(31,5,'pr','OTHERS','pr','-'),(32,5,'--host','OTHERS','host',''),(33,5,'komputer2,komputer3','OTHERS','host','-'),(34,5,'komputer2','OTHERS','host',''),(35,5,'komputer3','OTHERS','host',''),(36,5,'8','OTHERS','Process','-'),(38,6,'\"\"','INPUT','running','matmul.out'),(39,5,'>>','OUTPUT','output into text file','result.txt'),(40,6,'>>','OUTPUT','output into text file',''),(41,7,'-np','OTHERS','Number of Processes',''),(42,7,'7','OTHERS','',''),(43,8,'-o','OUTPUT','','fox'),(44,8,'\"\"','INPUT','','fox.c'),(45,8,'-lm','OTHERS','',''),(46,8,'-f','INPUT','','machine'),(47,7,'-f machine','OTHERS','','machine'),(49,7,'./fox','OTHERS','',''),(52,7,'fox','OTHERS','',''),(54,7,'>> hasil.txt','OTHERS','',''),(61,3,'em','OTHERS','',''),(67,5,'-np 16 --host komputer2,komputer3 /usr/local/gromacs/bin/gmx_mpi mdrun -v -deffnm pr','OTHERS','','');
/*!40000 ALTER TABLE `command_argument` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `application`
--
ALTER TABLE `application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `command_argument`
--
ALTER TABLE `command_argument`
  ADD PRIMARY KEY (`id`,`command_main_id`),
  ADD KEY `command_main_id` (`command_main_id`);

--
-- Indexes for table `command_main`
--
ALTER TABLE `command_main`
  ADD PRIMARY KEY (`id`,`pool_id`,`apps_id`),
  ADD KEY `apps_id` (`apps_id`),
  ADD KEY `pool_id` (`pool_id`);

--
-- Indexes for table `experiment`
--
ALTER TABLE `experiment`
  ADD PRIMARY KEY (`id`,`user_id`,`pool_id`,`apps_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `apps_id` (`apps_id`),
  ADD KEY `pool_id` (`pool_id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`id`,`experiment_id`),
  ADD KEY `experiment_id` (`experiment_id`);

--
-- Indexes for table `machine`
--
ALTER TABLE `machine`
  ADD PRIMARY KEY (`id`,`pool_id`),
  ADD KEY `pool_id` (`pool_id`);

--
-- Indexes for table `pool`
--
ALTER TABLE `pool`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `application`
--
ALTER TABLE `application`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `command_argument`
--
ALTER TABLE `command_argument`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `command_main`
--
ALTER TABLE `command_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `experiment`
--
ALTER TABLE `experiment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=303;
--
-- AUTO_INCREMENT for table `machine`
--
ALTER TABLE `machine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pool`
--
ALTER TABLE `pool`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `command_argument`
--
ALTER TABLE `command_argument`
  ADD CONSTRAINT `command_argument_ibfk_1` FOREIGN KEY (`command_main_id`) REFERENCES `command_main` (`id`),
  ADD CONSTRAINT `command_argument_ibfk_2` FOREIGN KEY (`command_main_id`) REFERENCES `command_main` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `command_argument_ibfk_3` FOREIGN KEY (`command_main_id`) REFERENCES `command_main` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `command_argument_ibfk_4` FOREIGN KEY (`command_main_id`) REFERENCES `command_main` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `command_main`
--
ALTER TABLE `command_main`
  ADD CONSTRAINT `command_main_ibfk_1` FOREIGN KEY (`pool_id`) REFERENCES `pool` (`id`),
  ADD CONSTRAINT `command_main_ibfk_2` FOREIGN KEY (`apps_id`) REFERENCES `application` (`id`),
  ADD CONSTRAINT `command_main_ibfk_3` FOREIGN KEY (`apps_id`) REFERENCES `application` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `command_main_ibfk_4` FOREIGN KEY (`pool_id`) REFERENCES `pool` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `command_main_ibfk_5` FOREIGN KEY (`pool_id`) REFERENCES `pool` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `experiment`
--
ALTER TABLE `experiment`
  ADD CONSTRAINT `experiment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`username`),
  ADD CONSTRAINT `experiment_ibfk_2` FOREIGN KEY (`pool_id`) REFERENCES `pool` (`id`),
  ADD CONSTRAINT `experiment_ibfk_3` FOREIGN KEY (`apps_id`) REFERENCES `application` (`id`),
  ADD CONSTRAINT `experiment_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`username`) ON DELETE CASCADE,
  ADD CONSTRAINT `experiment_ibfk_5` FOREIGN KEY (`apps_id`) REFERENCES `application` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `experiment_ibfk_6` FOREIGN KEY (`pool_id`) REFERENCES `pool` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `experiment_ibfk_7` FOREIGN KEY (`pool_id`) REFERENCES `pool` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `job`
--
ALTER TABLE `job`
  ADD CONSTRAINT `job_ibfk_1` FOREIGN KEY (`experiment_id`) REFERENCES `experiment` (`id`),
  ADD CONSTRAINT `job_ibfk_2` FOREIGN KEY (`experiment_id`) REFERENCES `experiment` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `job_ibfk_3` FOREIGN KEY (`experiment_id`) REFERENCES `experiment` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `machine`
--
ALTER TABLE `machine`
  ADD CONSTRAINT `machine_ibfk_1` FOREIGN KEY (`pool_id`) REFERENCES `pool` (`id`),
  ADD CONSTRAINT `machine_ibfk_2` FOREIGN KEY (`pool_id`) REFERENCES `pool` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `machine_ibfk_3` FOREIGN KEY (`pool_id`) REFERENCES `pool` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
