# Condor Cloud

> Originally created by Dennis Pratama Kamah, Fasilkom UI, 2017

Portal manajemen sumber daya HPC dengan load balancer dan job sceduler berbasiskan high throughput condor

## Installation

Softwares:

1. Docker CE https://docs.docker.com/install/ 
2. Docker Compose https://docs.docker.com/compose/

Required ports:

1. `80` (Nginx)
2. `8989` (MySql. Configurable from `.env`)

Steps:

1. Copy `.env.example` to `.env`: replace `NGINX_HOST` with server domain/IP address, and change MySQL passwords
2. Build and run servers: `docker-compose up -d` (requires internet to download Docker images)
3. Open from browser `http://localhost/condor_cloud`
4. Login normally with default username/password `admin/1234`
5. Stop servers: `docker-compose down -v`

Note:

1. This application uses `ssh` and `sshpass` to connect to other machines. So when adding Pools or Machines, make sure they have port `22` accessible through this application's host

## Citation

```
@thesis{kamah2017thesis,
	author={Dennis Pratama {Kamah}},
    	title={Implementasi portal manajemen sumber daya HPC dengan load balancer dan job sceduler berbasiskan high throughput condor},
	school={Universitas Indonesia},
	year={2017},
}
```
