<?php
	class Components
	{
		var $type;
		var $connection;

		function __construct($type1) 
		{
   			$this->type = $type1;
   			$server = getenv('MYSQL_HOST');
			$username = getenv('MYSQL_USER');
			$password = getenv('MYSQL_PASSWORD');
			$database = getenv('MYSQL_DATABASE');
   			$this->connection = mysqli_connect($server, $username, $password, $database);
		}

		function process($sql)
		{
			if(mysqli_query($this->connection, $sql)) {
				return true;
			} else {
				// var_dump(mysqli_error($this->connection));
				return false;
			}
		}

		function create($columns, $values)
		{
			$sql = "INSERT INTO " . $this->type . " (" . $columns . ") VALUES " . " (" . $values . ")";
			return $this->process($sql);
		}

		function read($option, $condition)
		{
			if($condition != "") {
				$sql = "SELECT " . $option . " FROM " . $this->type . " WHERE " . $condition;
			} else {
				$sql = "SELECT " . $option . " FROM " . $this->type;
			}
			if($result = mysqli_query($this->connection, $sql)) {
				return $result;
			} else {
				return null;
			}
		}

		function delete($condition)
		{
			$sql = "DELETE FROM " . $this->type . " WHERE " . $condition;
			return $this->process($sql);
		}

		function update($set, $condition) 
		{
			$sql = "UPDATE " . $this->type . " SET " . $set . " WHERE " . $condition;
			return $this->process($sql);
		}

		function get_last_inserted_id()
		{
			return mysqli_insert_id($this->connection);
		}
	}
?>
