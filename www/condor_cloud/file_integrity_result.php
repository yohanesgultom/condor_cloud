<?php
  header("Expires: Wed, 31 Dec 2025 11:59:59 GMT");
  header("Cache-Control: no-cache");
  header("Pragma: no-cache");
  
  session_start();
?>

<!DOCTYPE html>
	<head>
		<title>Condor Cloud - File Integrity Result</title>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
		<meta name="author" content="Dennis Pratama Kamah, 1306464562, Fakultas Ilmu Komputer, Universitas Indonesia"/>
		<link href="libraries/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
		<script src="libraries/js/jquery-3.1.1.min.js" type="text/javascript"></script>
		<script src="libraries/js/materialize.min.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="container">
			<table class="striped">
				<thead>
					<tr>
						<th data-field="name">Location</th>
						<th data-field="name">File Name</th>
						<th data-field="path">File Path</th>
						<th data-field="sha">SHA-1 Hash of File</th>
						<th data-field="size">Size of File</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Portal</td>
						<td><?php echo $_SESSION['portal_file'];?></td>
						<td><?php echo $_SESSION['portal_path'];?></td>
						<td><?php echo $_SESSION['portal_file_sha'];?></td>
						<td><?php echo $_SESSION['portal_size'];?> byte</td>
					</tr>
					<tr>
						<td>Head Node</td>
						<td><?php echo $_SESSION['headNode_file'];?></td>
						<td><?php echo $_SESSION['headNode_path'];?></td>
						<td><?php echo $_SESSION['headNode_file_sha'];?></td>
						<td><?php echo $_SESSION['headNode_size'];?> byte</td>
					</tr>
					<tr>
						<td>HPC Resource</td>
						<td><?php echo $_SESSION['hpc_file'];?></td>
						<td><?php echo $_SESSION['hpc_path'];?></td>
						<td><?php echo $_SESSION['hpc_file_sha'];?></td>
						<td><?php echo $_SESSION['hpc_size'];?> byte</td>
					</tr>
				</tbody>
			</table>
			<h4>Status :</h4>
			<?php echo $_SESSION['content_check'];?>
			<?php echo $_SESSION['size_check'];?>
			
			<?php
				session_unset(); 
				session_destroy(); 
			?>
			
			<br>
			<a href="http://<?php echo $_SERVER['SERVER_NAME'];?>/condor_cloud/file_integrity.php" class="waves-effect waves-light red btn">Back to File Integrity Page</a>
		</div>
	</body>
</html>




	