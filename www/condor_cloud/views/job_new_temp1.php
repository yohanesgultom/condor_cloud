<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} 

  include 'templates/header.php';
  require "../models/Components.php";

  $ex_id = $_GET['ex_id'];
  $temp = new Components('experiment'); 
  $result = $temp->read("*", "id = '$ex_id'");
  while($row = mysqli_fetch_assoc($result)) {
	$pool_id = $row['pool_id'];
	$apps_id = $row['apps_id'];
	}
?>

<script>
$(document).ready(function(){
	$("#add_argument").click(function(){
    	var id = $("#command_id").val();
		var type = $("#command_type").val();
		var ex_id = $("#ex_id").val();
		$.post("../controllers/tempShowArgument.php",
    	{
        	command_id: id,
        	command_type: type,
			experiment_id: ex_id
    	},
    	function(data, status){
       		$("#special").append(data);
    	});
	});

	$("#remove_argument").click(function(){
    	$(".additional").remove();
	});
})
</script>

<div class="container">
	<h2>Choose arguments for your job :</h2>
	<?php
		$temp3 = new Components('command_main');
		$id = $_GET['job_command'];
		$result3 = $temp3->read("*", "id='$id'");
		while($row = mysqli_fetch_assoc($result3)) {
			$string = $row['string'];
		}
	?>
	<div class="row">
		<div class="input-field col s12">
			<input readonly value="<?php echo $id; ?>" id="command_id" name="command_id" type="text" class="validate">
			<label for="command_id">Command ID</label>
		</div>
	</div>
	<div class="row">
		<div class="input-field col s6">
          	<select class="icons" name="command_type" id="command_type">
				<option value="" disabled selected>Choose your option</option>
				<?php
					$temp2 = new Components('command_argument');
					$result2 = $temp2->read("DISTINCT argument_type", "command_main_id = '$id'");
					while($row = mysqli_fetch_assoc($result2)) {
						echo "<option value=\"" . $row['argument_type'] . "\" class=\"circle\">" . $row['argument_type'] . "</option>";
					}
				?>
			</select>
			<label>Argument Type</label>
        </div>
		<div class="input-field col s6">
          	<button id="add_argument" class="waves-effect waves-light btn"><i class="material-icons right">library_add</i>Add Argument</button><br><br>
          	<button id="remove_argument" class="waves-effect waves-light btn"><i class="material-icons right">shuffle</i>Remove All Arguments</button>
        </div>
    </div>
    <form class="col s12" action="../controllers/job_new.php" method="post">
		<input hidden value="<?php echo $pool_id; ?>" name="pool_id" type="text">
		<div class="row">
			<div class="input-field col s12">
				<input readonly value="<?php echo $_GET['ex_id']; ?>" id="ex_id" name="ex_id" type="text" class="validate">
				<label for="ex_id">Experiment ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input readonly value="<?php echo $string; ?>" id="command_name" name="command_name" type="text" class="validate">
				<label for="command_name">Command Name</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<select class="icons" name="execution" id="execution">
					<option value="Remote" class="circle" selected>Remote</option>
					<option value="Local" class="circle">Local</option>
				</select>
				<label for="execution">Command Execution</label>
			</div>
		</div>
		<div class="row" id="special">
			<div class="input-field col s12" >
				<input readonly value="<?php echo $_GET['description']; ?>" id="job_description" name="job_description" type="text" class="validate">
				<label for="job_description">Job Description</label>
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">input</i>Submit Job</button>
		<a href="http://<?php echo $_SERVER['SERVER_NAME'];?>/condor_cloud/views/job_new.php?ex_id=<?php echo $_GET['ex_id']; ?>" class="red darken-1 waves-effect waves-green btn-flat">Back to command</a>
    </form>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
