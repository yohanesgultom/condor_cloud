<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	} 

  include 'templates/header.php';
  require "../models/Components.php";
?>

<br>
<div class="container">
	<h2>Create New Machine for Pool <?php echo $_GET['name']; ?></h2>
	<div class="row">
    <form class="col s12" action="../controllers/machine_new.php" method="post">
		<div class="row">
			<div class="input-field col s12">
				<input readonly value="<?php echo $_GET['id'];?>" name="pool_id" type="text">
				<label for="pool_id">Pool ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input readonly value="<?php echo $_GET['name'];?>" name="pool_name" type="text">
				<label for="pool_name">Pool Name</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input readonly value="Computing Node" name="role" type="text">
				<label for="pool_id">Machine Role</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input name="ip_address" type="text">
				<label for="ip_address">Machine IP Address</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input name="hostname" id="hostname" type="text">
				<label for="hostname">Machine Hostname</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input name="root_username" id="root_username" type="text">
				<label for="root_username">Root Username</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input name="root_password" id="root_password" type="password">
				<label for="root_password">Root Password</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<textarea id="textarea1" name="description" class="materialize-textarea"></textarea>
				<label for="textarea1">Machine Description</label> 
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">input</i>Create</button>
    </form>
  </div>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
