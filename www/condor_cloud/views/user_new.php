<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	}

  include 'templates/header.php';
  require "../models/Components.php";
?>

<br>
<div class="container">
	<h2>Create User</h2>
	<div class="row">
    <form class="col s12" action="../controllers/user_new.php" method="post">
		<div class="row">
			<div class="input-field col s12">
				<input id="id" name="id" type="text" class="validate" required>
				<label for="id">Username</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="password" name="password" type="password" class="validate" required>
				<label for="name">Password</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="name" name="name" type="text" class="validate">
				<label for="name">Name</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<select class="icons" name="role">
					<option value="" disabled selected>Choose your option</option>
					<option value="admin" class="circle">Admin</option>
					<option value="researcher" class="circle">Researcher</option>
				</select>
				<label>Role</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="email" name="email" type="email" class="validate">
				<label for="email">Email</label>
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">input</i>Create</button>
    </form>
  </div>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
