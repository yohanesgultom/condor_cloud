<?php
	session_start();

  	if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} 
	
	if(!(isset($_GET['id']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/experiment.php");
	} 

  	include 'templates/header.php';
  	require "../models/Components.php";
	
	if(isset($_SESSION['error'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal red\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['error'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['error']);
  	} else if(isset($_SESSION['success'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal green lighten-1\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['success'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['success']);
  	}
	
	if(isset($_SESSION['job_log'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#viewFile').modal('open');
				});
        	</script>
			<div id=\"viewFile\" class=\"modal teal lighten-1\"> 
				<div class=\"modal-content\">
					<pre>" . $_SESSION['job_log'] . "</pre>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['job_log']);
  	} 
?>

<br>

<br>
<div class="container">
	<nav>
		<div class="nav-wrapper teal">
			<div class="col s12">
				<a href="home.php" class="breadcrumb">Home</a>
				<a href="experiment.php" class="breadcrumb">Experiment</a>
				<a href="<?php echo "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/job.php?id=" . $_GET['id'] . "&name=" . $_GET['name'] ."\"" ?>" class="breadcrumb"><?php echo $_GET['name'];?></a>
			</div>
		</div>
	</nav>
	<br>
	<div class="row">
		<div class="col s6">
			<div class="input-field">
				<input disabled id="search" type="search">
				<label for="search"><i class="material-icons">search</i> Search Job</label>
			<i class="material-icons">close</i>
        </div>
		</div>
		<div class="col s6">
			<br>
			<a href="http://<?php echo $_SERVER['SERVER_NAME']; ?>/condor_cloud/views/job_new.php?ex_id=<?php echo $_GET['id']; ?>" class="waves-effect waves-light btn">Create/Add a New Job</a>
		</div>
	</div>
	<table class="striped">
	<thead>
		<th>Job Name</th>
		<th>Condor ID</th>
		<th>Starting Time</th>
		<th>Finished Time</th>
		<th>Description</th>
		<th>Execution</th>
		<th>Status</th>
	</thead>
		<tbody>	<?php
	$ex_id = $_GET['id'];
	
	$temp4 = new Components("experiment");
	$result4 = $temp4->read("*", "id = '$ex_id'");
	while($row = mysqli_fetch_assoc($result4)){
		$pool_id = $row['pool_id'];
	}
	
	$temp3 = new Components("machine");
	$result3 = $temp3->read("*", "pool_id = '$pool_id' AND role = 'Head Node'");
	while($row = mysqli_fetch_assoc($result3)){
		$ip = $row['ip_address'];
		$root_username = $row['root_username'];
		$root_password = $row['root_password'];
		$local_path = $row['local_path'];
	}
	
	//Update Status of Job
	$temp3 = new Components("job");
	$result3 = $temp3->read("*", "experiment_id = '$ex_id'");
	
	while($row = mysqli_fetch_assoc($result3)) {
		$temp23 = new Components('job');
		if($row['execution'] == "Remote") {
			$command = 'condor_q';
			$output = shell_exec('/usr/bin/sudo -S /usr/bin/sshpass -p "' . $root_password . '" ssh -o StrictHostKeyChecking=no ' . $root_username . '@' . $ip . ' ' . $command);
	
			$command = '> content.txt && /bin/chmod 777 content.txt';
			shell_exec($command);
	
			$myfile = fopen("content.txt", "w");
			fwrite($myfile, $output);
			fclose($myfile);
		
			$myfile = fopen("content.txt", "r");
			while(!feof($myfile)) {
				$line = fgets($myfile);
				$array = preg_split('#\s+#', $line);
				
				$job_id = $row['id'];
				if(count($array) >= 9 && $array[1] != "ID" && $array[0] == "") {
					$temp = explode(".", $array[1]);
					if($temp[0] == $row['condor_id']) {
						if($array[6] == "R" || $array[6] == "<" || $array[6] == ">") {
							$status = "running";
						} else if($array[6] == "H") {
							$status = "halt";
						} else {
							$status = "idle";
						} 
						$result23 = $temp23->update("status = '$status'", "id = '$job_id'");
					} 
				} 
			}
			fclose($myfile);
		
			$command = 'rm content.txt';
			shell_exec($command);
		} else {
			$job_id = $row['id'];
			$result23 = $temp23->update("status = 'finished'", "id = '$job_id'");
		}
	}
	
	
	//Update Finished Time of Job
	$temp2 = new Components("job");
	$result2 = $temp2->read("*", "experiment_id = '$ex_id'");
	while($row = mysqli_fetch_assoc($result2)) {
		if($row['execution'] == "Remote") {
			$command = 'condor_q -analyze ' . $row['condor_id'];
			$output = shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" ssh -o StrictHostKeyChecking=no ' . $root_username . '@' . $ip . ' ' . $command);
			
			$temp = preg_split('#\s+#', $output);

			$exist = 0;
			for($i = 0; $i < count($temp); $i++) {
				if($temp[$i] == "running." || $temp[$i] == "halt." || $temp[$i] == "idle.") {
					$exist = 1;
					break;
				}
			}
			
			$job_id = $row['id'];
			if($exist == 0) {
				$temp45 = new Components("job");
				$result45 = $temp45->update("status = 'finished'", "id = '$job_id'");
			
				$command = 'cat ' . $local_path . '/' . $ex_id . '/' . 'data_' . $job_id . '.log';
				$output = shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" ssh -o StrictHostKeyChecking=no ' . $root_username . '@' . $ip . ' ' . $command);
				$temp = preg_split('#\s+#', $output);
				
				$temp2 = explode("/", $temp[2]);
				if($temp[47] == "Image") {
					$temp3 = explode("/", $temp[68]);
				} else {
					$temp3 = explode("/", $temp[45]);
				}
				
				
				$start_time = date("Y");
				$finish_time = date("Y");
				$start_time .= "-" . $temp2[0] . "-" . $temp2[1] . " " . $temp[3];
				if($temp[47] == "Image") {
					$finish_time .= "-" . $temp3[0] . "-" . $temp3[1] . " " . $temp[69];
				} else {
					$finish_time .= "-" . $temp3[0] . "-" . $temp3[1] . " " . $temp[46];
				}
				
				$result44 = $temp45->update("starting_time = '$start_time', finished_time = '$finish_time'", "id = '$job_id'");
			}	
		}
	}
?>

<?php
	$temp = new Components("job");
	$id = $_GET['id'];
	$result = $temp->read("*", "experiment_id = '$id'");
	if(mysqli_num_rows($result) > 0) {
    	while($row = mysqli_fetch_assoc($result)) {
			echo "<tr>";
			$id = $row["id"];
			echo "<td>" . $row["string"] . "</td>";
			echo "<td>" . $row["condor_id"] . "</td>";
			echo "<td>" . $row["starting_time"] . "</td>";
			echo "<td>" . $row["finished_time"] . "</td>";
			echo "<td>" . $row["description"] . "</td>";
			
			$status = $row["status"];
			echo "<td>" . $row['execution'] . "</td>";
			echo "<td> <span class=\"new badge ";
			if($status == "running") {
				echo "blue\" data-badge-caption=\"running";
			} else if($status == "finished") {
				echo "green\" data-badge-caption=\"finished";
			} else if($status == "halt") {
				echo "yellow\" data-badge-caption=\"halt";
			} else if($status == "idle") {
				echo "white\" data-badge-caption=\"idle";
			} else if($status == "forced stop") {
				echo "red\" data-badge-caption=\"forced stop";
			}
			
			echo "\"></span></td>";
			echo "<td>";
			if($status == "running" || $status == "halt" || $status == "idle") {
				echo "<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/controllers/job_stop.php?job_id=" . $id . "&ex_id=" . $_GET['id'] . "&ex_name=" . $_GET['name'] . "\" class=\"chip\"><i class=\"material-icons tiny\">report_problem</i>Stop Job</a>";
			}
			if($status == "finished" || $status == "forced stop") {
				echo "<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/temp_job_delete.php?id=" . $id . "&name=" . $row["string"] . "&ex_id=" . $_GET['id'] . "&ex_name=" . $_GET['name'] . "\" class=\"chip\"><i class=\"material-icons tiny\">delete</i>Delete Job</a>";
			}
			if($row['execution'] == "Remote") {
				echo "<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/controllers/job_log.php?job_id=" . $id . "&ex_id=" . $_GET['id'] . "&ex_name=" . $_GET['name'] . "\" class=\"chip\"><i class=\"material-icons tiny\">search</i>See Log</a>";
				echo "<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/controllers/job_error.php?job_id=" . $id . "&ex_id=" . $_GET['id'] . "&ex_name=" . $_GET['name'] . "\" class=\"chip\"><i class=\"material-icons tiny\">search</i>See Error</a>";
			}
			echo "</td>";
			echo "</tr>";
    	}
	} else {
    	echo "0 results";
	}
	
?>
		</tbody>
	</table>	
</div>
<br>
<?php
  include 'templates/footer.php';
?>
