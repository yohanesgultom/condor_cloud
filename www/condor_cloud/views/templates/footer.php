    <footer class="page-footer teal">
      <div class="container">
        <div class="row">
          <div class="col l6 s12">
            <h5 class="white-text">Insilico Laboratory</h5>
            <p class="grey-text text-lighten-4">What a great experience in doing simulation with Amber and Gromacs in Cluster and GPU Computing Resources</p>
          </div>
        </div>
      </div>
      <div class="footer-copyright">
        <div class="container">
          <?php echo date("Y");?> &#169; Copyright Laboratorium Grid &#38; HPC Fasilkom UI. All Rights Reserved. Powered by <a href="http://materializecss.com/" style="text-decoration: none;" target="_blank">Materialize</a>
        </div>
      </div>
    </footer>
  </body>
</html>
