<?php
  header("Expires: Wed, 31 Dec 2025 11:59:59 GMT");
  header("Cache-Control: no-cache");
  header("Pragma: no-cache");
?>

<!DOCTYPE html>
  <head>
    <title>Condor Cloud - HPC Portal</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <meta name="description" content="Condor Cloud merupakan portal untuk melakukan komputasi yang membutuhkan high performance resources"/>
    <meta name="keywords" content="condor, HPC, amber, gromacs"/>
    <meta name="author" content="Dennis Pratama Kamah, 1306464562, Tugas Akhir, Fakultas Ilmu Komputer, Universitas Indonesia"/>
    <link href="../libraries/css/icons.css" type="text/css" rel="stylesheet">
    <link href="../libraries/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="../libraries/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <script src="../libraries/js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="../libraries/js/materialize.min.js" type="text/javascript"></script>
    <script src="../libraries/js/init.js" type="text/javascript"></script>
	<script src="../libraries/js/add.js" type="text/javascript"></script>
  </head>
  <body>
      <nav class="white" role="navigation">
        <div class="nav-wrapper container">
          <a id="logo-container" href="home.php" class="brand-logo right"><i class="material-icons">cloud</i>Condor Cloud</a>
          <ul id=\"nav-mobile\" class=\"left hide-on-med-and-down\">
<?php
  if(isset($_SESSION['username'])) {
    if($_SESSION['role'] == 'admin') {
      echo "
        <ul id=\"dropdown1\" class=\"dropdown-content\">
          <li><a href=\"edit_own_profile.php\">Edit Profile</a></li>
          <li class=\"divider\"></li>
          <li><a href=\"../controllers/logout.php\">LOGOUT</a></li>
        </ul>
        <li><a class=\"dropdown-button\" href=\"#!\" data-activates=\"dropdown1\">Hi, " . $_SESSION['username'] ."<i class=\"material-icons right\">arrow_drop_down</i></a></li>
		<li><a href=\"experiment.php\">Experiment</a></li>
        <li><a href=\"pool.php\">Pool</a></li>
        <li><a href=\"application.php\">Application</a></li>
        <li><a href=\"command.php\">Command</a></li>
		<li><a href=\"user.php\">Users</a></li>
    ";
    } else {
      echo "
        <ul id=\"dropdown1\" class=\"dropdown-content\">
          <li><a href=\"edit_own_profile.php\">Edit Profile</a></li>
          <li class=\"divider\"></li>
          <li><a href=\"../controllers/logout.php\">LOGOUT</a></li>
        </ul>
        <li><a class=\"dropdown-button\" href=\"#!\" data-activates=\"dropdown1\">Hi, " . $_SESSION['username'] ."<i class=\"material-icons right\">arrow_drop_down</i></a></li>
        <li><a href=\"experiment.php\">Experiment</a></li>
    ";
    }
  }
?>

          </ul>
          <ul id="nav-mobile" class="side-nav">
            <li><a href="#">Navbar Link</a></li>
          </ul>
          
        </div>
      </nav>
