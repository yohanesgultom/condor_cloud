<?php
	session_start();

  	if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	}

	if(!(isset($_GET['id']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/command.php");
	} 

  	include 'templates/header.php';
  	require "../models/Components.php";
	
	if(isset($_SESSION['error'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal red\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['error'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['error']);
  	} else if(isset($_SESSION['success'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal green lighten-1\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['success'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['success']);
  	}
?>

<br>

<br>
<div class="container">
	<nav>
		<div class="nav-wrapper teal">
			<div class="col s12">
				<a href="home.php" class="breadcrumb">Home</a>
				<a href="command.php" class="breadcrumb">Command</a>
				<a href="<?php echo "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/argument.php?id=" . $_GET['id'] . "&name=" . $_GET['name'] ."\""; ?>" class="breadcrumb"><?php echo $_GET['name'];?></a>
			</div>
		</div>
	</nav>
	<br>
	<div class="row">
		<div class="col s6">
			<div class="input-field">
				<input disabled id="search" type="search">
				<label for="search"><i class="material-icons">search</i> Search Argument</label>
			<i class="material-icons">close</i>
        </div>
		</div>
		<div class="col s6">
			<br>
			<a href="<?php echo "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/argument_new.php?id=" . $_GET['id']; ?>" class="waves-effect waves-light btn">Create/Add a New Argument</a>
		</div>
	</div>
	<table class="striped">
	<thead>
		<th>String</th>
		<th>Argument Type</th>
		<th>Description</th>
		<th>Content Sample</th>
	</thead>
		<tbody>	
<?php
	$temp = new Components("command_argument");
	$id = $_GET['id'];
	$result = $temp->read("*", "command_main_id = '$id'");
	if(mysqli_num_rows($result) > 0) {
    	while($row = mysqli_fetch_assoc($result)) {
			echo "<tr>";
			$id = $row['id'];
			$string = $row["string"];
			echo "<td>" . $string . "</td>";
			echo "<td>" . $row["argument_type"] . "</td>";
			echo "<td>" . $row["description"] . "</td>";
			echo "<td>" . $row["content_sample"] . "</td>";
			echo "<td> 
				<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/argument_edit.php?id=" . $id . "\" class=\"chip\"><i class=\"material-icons tiny\">mode_edit</i>Edit</a>
				<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/temp_argument_delete.php?id=" . $id . "&name=" . $string . "&co_id=" . $row['command_main_id'] . "&co_name=" . $_GET['name'] ."\" class=\"chip\"><i class=\"material-icons tiny\">delete</i>Delete</a>
			</td>";
			echo "</tr>";
    	}
	} else {
    	echo "0 results";
	}
	
?>
		</tbody>
	</table>	
</div>
<br>
<?php
  include 'templates/footer.php';
?>
