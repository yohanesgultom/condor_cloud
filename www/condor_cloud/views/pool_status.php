<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
  } else {
	if($_SESSION['role'] != 'admin') {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
	}
  }

  include 'templates/header.php';
  require "../models/Components.php";
  
   if(isset($_SESSION['error'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal red\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['error'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['error']);
  	} else if(isset($_SESSION['success'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal green lighten-1\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['success'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['success']);
  	}
?>

<br>
<br>
<div class="container">
<?php
	$pool_id = $_GET['id'];
	
	$temp = new Components("machine");
	$result = $temp->read("*", "pool_id = '$pool_id' AND role = 'Head Node'");
	while($row = mysqli_fetch_assoc($result)) {
		$head_username = $row['root_username'];
		$head_password = $row['root_password'];
		$ip = $row['ip_address'];
	}
	
	$command = 'condor_status';
	$output = shell_exec('/usr/bin/sudo -S /usr/bin/sshpass -p "' . $head_password . '" ssh -o StrictHostKeyChecking=no ' . $head_username . '@' . $ip . ' ' . $command);
	
	$command = '> content.txt && /bin/chmod 777 content.txt';
	shell_exec($command);
	
	$myfile = fopen("content.txt", "w");
	fwrite($myfile, $output);
	fclose($myfile);
	
	echo "<br>";
	echo "<h4>Resources Node and Its Slots located in this pool</h4>";
	echo "<br>";
	echo "<table class=\"striped\">";
	echo "
		<thead>
        	<tr>
            	<th data-field=\"name\">Name</th>
              	<th data-field=\"os\">Operating System</th>
              	<th data-field=\"user\">Architecture</th>
              	<th data-field=\"group\">State</th>
              	<th data-field=\"size\">Activity</th>
              	<th data-field=\"time\">Load Average</th>
				<th data-field=\"time\">Memory</th>
              	<th data-field=\"operations\">Activity Time</th>
          	</tr>
        </thead>
        <tbody>
	";
	
	$myfile = fopen("content.txt", "r");
	while(!feof($myfile)) {
		echo "<tr>";
		$line = fgets($myfile);
		$array = preg_split('#\s+#', $line);
		if(count($array) >= 9 && $array[0] != "Name") {
			echo "<td>" . $array[0] . "</td>";
			echo "<td>" . $array[1] . "</td>";
			echo "<td>" . $array[2] . "</td>";
			echo "<td>" . $array[3] . "</td>";
			echo "<td>" . $array[4] . "</td>";
			echo "<td>" . $array[5] . "</td>";
			echo "<td>" . $array[6] . "</td>";
			echo "<td>" . $array[7] . "</td>";
			echo "<td>" . $array[8] . "</td>";
		}
		echo "</tr>";
	}
	fclose($myfile);

	echo "
		</tbody>
		</table>
	";

	$command = 'rm content.txt';
	shell_exec($command);
	
?>

<a href="http://<?php echo $_SERVER['SERVER_NAME'];?>/condor_cloud/views/pool_status.php?id=<?php echo $_GET['id'];?>" class="green darken-1 waves-effect waves-green btn-flat">Refresh</a>
<a href="http://<?php echo $_SERVER['SERVER_NAME'];?>/condor_cloud/views/pool.php" class="red darken-1 waves-effect waves-green btn-flat">Back to previous</a>
</div>
<br>
<?php
  include 'templates/footer.php';
?>