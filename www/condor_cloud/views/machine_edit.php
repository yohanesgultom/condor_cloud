<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	}

  if(!(isset($_GET['id']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/command.php");
	}

  include 'templates/header.php';
  require "../models/Components.php";

  $id = $_GET['id'];
  $temp = new Components("machine");
  $result = $temp->read("*", "id = '$id'");
  if(mysqli_num_rows($result) > 0) {
  	while($row = mysqli_fetch_assoc($result)) {
  		$id = $row['id'];
		$pool_id = $row['pool_id'];
		$ip_address = $row['ip_address'];
  		$hostname = $row['hostname'];
		$root_username = $row['root_username'];
		$root_password = $row['root_password'];
		$role = $row['role'];
		$description = $row['description'];
		$local_path = $row['local_path'];
		$nfs_path = $row['nfs_path'];
  	}
  }
?>

<br>
<div class="container">
	<h2>Edit A Machine</h2>
	<div class="row">
    <form class="col s12" action="../controllers/machine_edit.php" method="post">
		<div class="row">
			<div class="input-field col s12">
				<input readonly value="<?php echo $id;?>" name="id" type="text">
				<label for="id">Machine ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input readonly value="<?php echo $pool_id;?>" name="pool_id" type="text">
				<label for="pool_id">Pool ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input value="<?php echo $_GET['po_name'];?>" name="pool_name" type="text">
				<label for="pool_name">Pool Name</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input readonly value="<?php echo $role;?>" name="role" type="text">
				<label for="pool_id">Machine Role</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input value="<?php echo $ip_address;?>" name="ip_address" type="text">
				<label for="ip_address">Machine IP Address</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input value="<?php echo $hostname;?>" name="hostname" id="hostname" type="text">
				<label for="hostname">Machine Hostname</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input value="<?php echo $root_username;?>" name="root_username" id="root_username" type="text">
				<label for="root_username">Root Username</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input value="<?php echo $root_password;?>" name="root_password" id="root_password" type="password">
				<label for="root_password">Root Password</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<textarea value="<?php echo $description;?>" id="textarea1" name="description" class="materialize-textarea"></textarea>
				<label for="textarea1">Machine Description</label> 
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input value="<?php echo $local_path;?>" name="local_path" id="local_path" type="text">
				<label for="local_path">Local Path</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input value="<?php echo $nfs_path;?>" name="nfs_path" id="nfs_path" type="text">
				<label for="nfs_path">NFS Path</label>
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">mode_edit</i>Edit</button>
    </form>
  </div>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
