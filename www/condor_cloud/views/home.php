<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
	header("Location: index.php");
  }

  include 'templates/header.php';

  if(isset($_SESSION['error'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal red\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['error'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['error']);
  	} else if(isset($_SESSION['success'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal green lighten-1\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['success'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['success']);
  	}
?>

<br>
<br>
<div class="container">
	<div class="row">
		<div class="col s6">
			<div class="card small">
				<div class="card-image">
					<img src="../uploads/images/experiments.png">
					<span class="card-title">Experiments</span>
				</div>
				<div class="card-content">
					<p>Organize your own experiments and jobs inside of it</p>
				</div>
				<div class="card-action">
					<a href="experiment.php">Go to your experiments</a>
				</div> 
			</div>
		</div><?php
	if($_SESSION['role'] == 'admin') {
		echo "
			<div class=\"col s6\">
				<div class=\"card small\">
					<div class=\"card-image\">
						<img src=\"../uploads/images/pools.png\">
						<span class=\"card-title black-text\">Pools</span>
					</div>
					<div class=\"card-content\">
						<p>Organize condor pools and resources inside of it</p>
					</div>
					<div class=\"card-action\">
						<a href=\"pool.php\">Go to Pools</a>
					</div> 
				</div>
			</div>
		";
  	}
?>
	</div>
<?php
	if($_SESSION['role'] == 'admin') {
		echo "
			<div class=\"row\">
				<div class=\"col s4\"></div>
				<div class=\"col s4\">
					<div class=\"card small\">
						<div class=\"card-image\">
							<img src=\"../uploads/images/user.jpg\">
							<span class=\"card-title black\">Users</span>
						</div>
						<div class=\"card-content\">
							<p>Organize users in this cloud (portal)</p>
						</div>
						<div class=\"card-action\">
							<a href=\"user.php\">Go to Users</a>
						</div> 
					</div>
				</div>
				<div class=\"col s4\"></div>
			</div>
			<div class=\"row\">
				<div class=\"col s6\">
					<div class=\"card small\">
						<div class=\"card-image\">
							<img src=\"../uploads/images/apps.jpg\">
							<span class=\"card-title black-text\">Applications</span>
						</div>
						<div class=\"card-content\">
							<p>Organize dynamic molecular apps, like Gromacs, etc</p>
						</div>
						<div class=\"card-action\">
							<a href=\"application.php\">Go to Applications</a>
						</div> 
					</div>
				</div>
				<div class=\"col s6\">
					<div class=\"card small\">
						<div class=\"card-image\">
							<img src=\"../uploads/images/command.png\">
							<span class=\"card-title black\">Commands</span>
						</div>
						<div class=\"card-content\">
							<p>Organize terminal commands in running experiments</p>
						</div>
						<div class=\"card-action\">
							<a href=\"command.php\">Go to Commands</a>
						</div> 
					</div>
				</div>
			</div>
		";
	}	
?>

</div>
<br>
<?php
  include 'templates/footer.php';
?>
