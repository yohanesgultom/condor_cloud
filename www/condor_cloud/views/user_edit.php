<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	}

  if(!(isset($_GET['id']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/command.php");
	}

  include 'templates/header.php';
  require "../models/Components.php";

  $id = $_GET['id'];
  $temp = new Components("users");
  $result = $temp->read("*", "username = '$id'");
  if(mysqli_num_rows($result) > 0) {
  	while($row = mysqli_fetch_assoc($result)) {
  		$name = $row['name'];
		$role = $row['role'];
  		$email = $row['email'];
  	}
  }
?>

<br>
<div class="container">
	<h2>Edit An User</h2>
	<div class="row">
    <form class="col s12" action="../controllers/user_edit.php" method="post">
		<div class="row">
			<div class="input-field col s12">
				<input readonly="readonly" value="<?php echo $id;?>" id="id" name="id" type="text">
				<label for="id">Username</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="password" name="password" type="password">
				<label for="id">Password</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input value="<?php echo $name;?>" id="name" name="name" type="text">
				<label for="name">Name</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<select class="icons" name="role">
					<option value="<?php echo $role;?>" disabled selected><?php echo $role;?></option>
					<option value="admin" class="circle">Admin</option>
					<option value="researcher" class="circle">Researcher</option>
				</select>
				<label>Role</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input value="<?php echo $email;?>" id="email" name="email" type="email">
				<label for="name">Email</label>
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">mode_edit</i>Edit</button>
    </form>
  </div>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
