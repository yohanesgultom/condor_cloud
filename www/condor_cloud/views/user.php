<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	}

  include 'templates/header.php';
  require "../models/Components.php";
  
  if(isset($_SESSION['error'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal red\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['error'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['error']);
  	} else if(isset($_SESSION['success'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal green lighten-1\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['success'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['success']);
  	}
?>

<div id="modal1" class="modal">
	<form action="../controllers/user_delete.php" method="post">
		<div class="modal-content">
			<h4 style="text-align:center;">Login</h4>
			<div class="row">
				<div class="input-field col s12">
					<input name="username" type="text" class="validate" required>
					<label for="username">Username</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input name="password" type="password" class="validate" required>
						<label for="password">Password</label>
					</div>
				</div>
		</div>
		<div class="modal-footer">
				<div class="center">
					<a class="red darken-1 modal-action modal-close waves-effect waves-green btn-flat">No</a>
					<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">lock</i>Yes</button>
				</div>
		</div>
	</form>
</div>

<br>
<br>
<div class="container">
	<nav>
		<div class="nav-wrapper teal">
			<div class="col s12">
				<a href="home.php" class="breadcrumb">Home</a>
				<a href="user.php" class="breadcrumb">Users</a>
			</div>
		</div>
	</nav>
	<br>
	<div class="row">
		<div class="col s6">
			<div class="input-field">
				<input disabled id="search" type="search">
				<label for="search"><i class="material-icons">search</i> Search User</label>
			<i class="material-icons">close</i>
        </div>
		</div>
		<div class="col s6">
			<br>
			<a href="user_new.php" class="waves-effect waves-light btn">Create/Add a New User</a>
		</div>
	</div>
	<table class="striped">
	<thead>
		<th>Username</th>
		<th>Name</th>
		<th>Role</th>
		<th>Email</th>
		<th>Operation</th>
	</thead>
		<tbody>	
<?php
	$temp = new Components("users");
	$result = $temp->read("*", "");
	if(mysqli_num_rows($result) > 0) {
    	while($row = mysqli_fetch_assoc($result)) {
			echo "<tr>";
			$id = $row["username"];
			echo "<td>" . $id . "</td>";
			echo "<td>" . $row["name"] . "</td>";
			echo "<td>" . $row["role"] . "</td>";
			echo "<td>" . $row["email"] . "</td>";
			echo "<td> 
				<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/user_edit.php?id=" . $id . "\" class=\"chip\"><i class=\"material-icons tiny\">mode_edit</i>Edit</a>
				<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/temp_user_delete.php?id=" . $id . "\" class=\"chip\"><i class=\"material-icons tiny\">delete</i>Delete</a>
			</td>";
			echo "</tr>";
    	}
	} else {
    	echo "0 results";
	}
?>
		</tbody>
	</table>	
</div>
<br>
<?php
  include 'templates/footer.php';
?>
