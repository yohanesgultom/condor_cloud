<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	}

  include 'templates/header.php';
  require "../models/Components.php";
?>

<br>
<div class="container">
	<h2>Create Pool</h2>
	<div class="row">
    <form class="col s12" action="../controllers/pool_new.php" method="post">
		<div class="row">
			<div class="input-field col s12">
				<input id="pool_name" name="pool_name" type="text" class="validate" required>
				<label for="pool_name">Pool Name</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<select class="icons" name="pool_type">
					<option value="" disabled selected>Choose your option</option>
					<option value="GPU" class="circle">GPU</option>
					<option value="Cluster" class="circle">Cluster</option>
				</select>
				<label>Pool Type</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<textarea id="textarea1" name="pool_description" class="materialize-textarea" type="text"></textarea>
				<label for="textarea1">Pool Description</label> 
			</div>
		</div>
		<h4>Head Node Machine Data</h4>
		<div class="row">
			<div class="input-field col s12">
				<input readonly value="Head Node" id="role" name="role" type="text" class="validate">
				<label for="role">Role</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="hostname" name="hostname" type="text" class="validate" required>
				<label for="hostname">Head Node Hostname</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="ip" name="ip" type="text" class="validate">
				<label for="ip">Head Node IP</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="username" name="username" type="text" class="validate">
				<label for="username">Root Username on Head Node</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="password" name="password" type="password" class="validate">
				<label for="password">Root Password on Head Node</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="nfs_path" name="nfs_path" type="text" class="validate">
				<label for="nfs_path">NFS Path on Head Node</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<textarea id="textarea2" name="head_description" class="materialize-textarea" type="text"></textarea>
				<label for="textarea2">Description of Head Node</label> 
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">input</i>Create</button>
    </form>
  </div>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
