<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	}

  include 'templates/header.php';
  require "../models/Components.php";
?>

<br>
<div class="container">
	<h2>Create Application</h2>
	<div class="row">
    <form class="col s12" action="../controllers/application_new.php" method="post">
		<div class="row">
			<div class="input-field col s12">
				<input id="name" name="name" type="text" class="validate">
				<label for="name">Application Name</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<textarea id="textarea1" name="description" class="materialize-textarea" type="text"></textarea>
				<label for="textarea1">Application Description</label> 
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">input</i>Create</button>
    </form>
  </div>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
