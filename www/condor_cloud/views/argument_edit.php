<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	}

  if(!(isset($_GET['id']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/command.php");
	}

  include 'templates/header.php';
  require "../models/Components.php";

  $id = $_GET['id'];
  $temp = new Components("command_argument");
  $result = $temp->read("*", "id = '$id'");
  if(mysqli_num_rows($result) > 0) {
  	while($row = mysqli_fetch_assoc($result)) {
		$command_main_id = $row['command_main_id'];
		$string = $row['string'];
		$argument_type = $row['argument_type'];
		$description = $row['description'];
		$content_sample = $row['content_sample'];
  	}
  }
?>

<br>
<div class="container">
	<h2>Edit An Argument</h2>
	<div class="row">
    <form class="col s12" action="../controllers/argument_edit.php" method="post">
		<div class="row">
			<div class="input-field col s12">
				<input readonly value="<?php echo $id;?>" id="id" name="id" type="text" readonly>
				<label for="id">Argument ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input readonly value="<?php echo $command_main_id;?>" id="command_main_id" name="command_main_id" type="text" readonly>
				<label for="command_main_id">Command Main ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input value="<?php echo $string;?>" id="string" name="string" type="text">
				<label for="string">String</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<select class="icons" name="argument_type">
					<option value="<?php echo $argument_type;?>" selected><?php echo $argument_type;?></option>
					<option value="INPUT" class="circle">INPUT</option>
					<option value="OUTPUT" class="circle">OUTPUT</option>
					<option value="OTHERS" class="circle">OTHERS</option>
				</select>
				<label>Argument Type</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input value="<?php echo $description;?>" id="description" name="description" type="text">
				<label for="description">Description</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input value="<?php echo $content_sample;?>" id="content_sample" name="content_sample" type="text">
				<label for="content_sample">Content Sample</label>
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">mode_edit</i>Edit</button>
    </form>
  </div>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
