<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
	header("Location: index.php");
  }

  include 'templates/header.php';
  require "../models/Components.php";
?>

<br>
<div class="container">
	<h2>Edit My Profile</h2>
	<div class="row">
    <form class="col s12" action="../controllers/edit_own_profile.php" method="post">
		<div class="row">
			<div class="input-field col s12">
				<input disabled value="<?php echo $_SESSION['username'];?>" name="user_id" type="text">
				<label for="disabled">User ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input disabled value="<?php echo $_SESSION['role'];?>" name="role" type="text">
				<label for="disabled">Role</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input placeholder="<?php echo $_SESSION['name'];?>" id="name" name="name" type="text" class="validate">
				<label for="name">Name</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input placeholder="<?php echo $_SESSION['email'];?>" id="email" name="email" type="email" class="validate">
				<label for="email">Email</label>
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">mode_edit</i>Edit</button>
    </form>
  </div>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
