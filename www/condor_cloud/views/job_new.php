<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} 

  include 'templates/header.php';
  require "../models/Components.php";

  $ex_id = $_GET['ex_id'];
  $temp = new Components('experiment'); 
  $result = $temp->read("*", "id = '$ex_id'");
  while($row = mysqli_fetch_assoc($result)) {
	$pool_id = $row['pool_id'];
	$apps_id = $row['apps_id'];
	}
?>

<br>
<div class="container">
	<h2>Create New Job</h2>
	<div class="row">
    <form class="col s12" action="../views/job_new_temp1.php" method="get">
		<div class="row">
			<div class="input-field col s12">
				<input readonly value="<?php echo $_GET['ex_id']; ?>" id="ex_id" name="ex_id" type="text" class="validate">
				<label for="ex_id">Experiment ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<textarea id="textarea1" name="description" class="materialize-textarea"></textarea>
				<label for="textarea1">Job Description</label> 
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<select class="icons" name="job_command">
					<option value="" disabled selected>Choose your option</option>
					<?php
						$temp2 = new Components('command_main');
						$result2 = $temp2->read("*", "pool_id = '$pool_id' AND apps_id = '$apps_id'");
						while($row = mysqli_fetch_assoc($result2)) {
							echo "<option value=\"" . $row['id'] . "\" class=\"circle\">" . $row['string'] . "</option>";
						}
					?>
				</select>
				<label>Job Main Command</label>
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">input</i>Continue</button>
    </form>
  </div>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
