<?php
  session_start();

  if(isset($_SESSION['username'])) {
	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
  }

  include 'templates/header.php';
  
  if(isset($_SESSION['error'])) {
	echo "
		<script>
			$(document).ready(function(){
				$('#modalError').modal('open');
			});
        </script>
		<div id=\"modalError\" class=\"modal red\"> 
			<div class=\"modal-content\">
				<h4 style=\"text-align:center;\">" . $_SESSION['error'] . "</h4>	
			</div>
			<div class=\"modal-footer\">
				<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
			</div>
		</div>
	";
	unset($_SESSION['error']);
  }
?>

	<div id="modal1" class="modal">
		<form action="../controllers/normal_login.php" method="post">
			<div class="modal-content">
				<h4 style="text-align:center;">Login</h4>
				<div class="row">
					<div class="input-field col s12">
						<input name="username" type="text" class="validate" required>
						<label for="username">Username</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input name="password" type="password" class="validate" required>
						<label for="password">Password</label>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="center">
					<a class="red darken-1 modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
					<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">lock</i>Login</button>
				</div>
			</div>
		</form>
	</div>

    <div id="index-banner" class="parallax-container">
		<div class="section no-pad-bot">
			<div class="container">
				<br><br>
				<h1 class="header center teal-text text-lighten-2">Condor HPC Portal</h1>
				<div class="row center">
					<h5 class="header col s12 light">A great portal for executing High Performance Job with Powerful Resources</h5>
				</div>
				<div class="row center">
					<a href="../controllers/sso_login.php" id="download-button" class="btn-large waves-effect waves-light teal lime accent-1 black-text text-darken-2">Login through SSO UI</a>&nbsp;&nbsp;
					<button data-target="modal1" class="btn-large waves-effect waves-light teal lighten-1">Login normally</button>
				</div>
				<br><br>
			</div>
		</div>
        <div class="parallax"><img src="../uploads/images/gambar1.jpg" alt="Unsplashed background img 1"></div>
    </div>

    <div class="container">
		<div class="section">
          <!--   Icon Section   -->
			<div class="row">
				<div class="col s12 m4">
					<div class="icon-block">
						<h2 class="center brown-text"><i class="material-icons">cloud_circle</i></h2>
						<h5 class="center">Dynamic Access</h5>
						<p class="light">Since the advances in using cloud infrastructure, our portal can be accessed from anywhere and anytime</p>
					</div>
				</div>
				<div class="col s12 m4">
					<div class="icon-block">
						<h2 class="center brown-text"><i class="material-icons">developer_board</i></h2>
						<h5 class="center">Condor as Middleware</h5>
						<p class="light">High Throughput Condor or Condor is such a fantastic load balancer and scheduler in balancing and scheduling a lot of contentful job without specifically determining suitable job and resources</p>
					</div>
				</div>
				<div class="col s12 m4">
					<div class="icon-block">
						<h2 class="center brown-text"><i class="material-icons">remove_red_eye</i></h2>
						<h5 class="center">Eye-Catching Interface</h5>
						<p class="light">Comfortability in Doing Molecular Simulation is our first priority so that researcher can finish the experiment beatifully.</p>
					</div>
				</div>
			</div>
		</div>
    </div>


    <div class="parallax-container valign-wrapper">
		<div class="section no-pad-bot">
			<div class="container">
				<div class="row center">
					<h5 class="header col s12 light">A tools in doing large molecular dynamic simulation with Amber and Gromacs</h5>
				</div>
			</div>
		</div>
		<div class="parallax"><img src="../uploads/images/gambar2.jpg" alt="Unsplashed background img 2"></div>
	</div>

	<div class="container">
		<div class="section">
			<div class="row">
				<div class="col s12 center">
					<h3><i class="mdi-content-send brown-text"></i></h3>
					<h4>Contact Us</h4>
					<p class="left-align light">
						<ul class="collection">
							<li class="collection-item">Computer Networks, Architecture and High Performance Computing Lab</li>
							<li class="collection-item">Gedung A Ruang 1231 Fakultas Ilmu Komputer Universitas Indonesia</li>
							<li class="collection-item">http://grid.ui.ac.id/</li>
						</ul>
					</p>
				</div>
			</div>
		</div>
	</div>
<?php
  include 'templates/footer.php';
?>
