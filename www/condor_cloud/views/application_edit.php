<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	}

  if(!(isset($_GET['id']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/application.php");
	}

  include 'templates/header.php';
  require "../models/Components.php";

  $id = $_GET['id'];
  $temp = new Components("application");
  $result = $temp->read("*", "id = '$id'");
  if(mysqli_num_rows($result) > 0) {
  	while($row = mysqli_fetch_assoc($result)) {
  		$name = $row['name'];
  		$description = $row['description'];
  	}
  }
?>

<br>
<div class="container">
	<h2>Edit An Application</h2>
	<div class="row">
    <form class="col s12" action="../controllers/application_edit.php" method="post">
		<div class="row">
			<div class="input-field col s12">
				<input readonly="readonly" value="<?php echo $id;?>" id="id" name="id" type="text">
				<label for="id">Experiment ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input placeholder="<?php echo $name;?>" id="name" name="name" type="text">
				<label for="name">Application Name</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<textarea id="textarea1" name="description" class="materialize-textarea" placeholder="<?php echo $description;?>"></textarea>
				<label for="textarea1">Experiment Description</label> 
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">mode_edit</i>Edit</button>
    </form>
  </div>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
