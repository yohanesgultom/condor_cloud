<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	}

  include 'templates/header.php';
  require "../models/Components.php";
?>

<br>
<div class="container">
	<h2>Create New Command</h2>
	<div class="row">
    <form class="col s12" action="../controllers/command_new.php" method="post">
		<div class="row">
			<div class="input-field col s12">
				<input id="string" name="string" type="text">
				<label for="string">Command name</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12 m6">
				<select class="icons" name="pool_id">
					<option value="" disabled selected>Choose your option</option>
					<?php
						$temp = new Components("pool");
						$result = $temp->read("*", "");
						while($row = mysqli_fetch_assoc($result)) {
							echo "<option value=\"" . $row['id'] . "\" class=\"circle\">" . $row['name'] . "</option>";
						}
					?>
				</select>
				<label>Pool ID</label>
			</div>
			<div class="input-field col s12 m6">
				<select class="icons" name="apps_id">
					<option value="" disabled selected>Choose your option</option>
					<?php
						$temp = new Components("application");
						$result = $temp->read("*", "");
						while($row = mysqli_fetch_assoc($result)) {
							echo "<option value=\"" . $row['id'] . "\" class=\"circle\">" . $row['name'] . "</option>";
						}
					?>
				</select>
				<label>Application ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<textarea id="textarea1" name="description" class="materialize-textarea"></textarea>
				<label for="textarea1">Command Description</label> 
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">input</i>Create</button>
    </form>
  </div>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
