<?php
  	session_start();

  	if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} 

	include 'templates/header.php';
	require "../models/Components.php";

	if(isset($_SESSION['error'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal red\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['error'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['error']);
  	} else if(isset($_SESSION['success'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal green lighten-1\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['success'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['success']);
  	}

  	if(isset($_SESSION['view_file'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#viewFile').modal('open');
				});
        	</script>
			<div id=\"viewFile\" class=\"modal teal lighten-1\"> 
				<div class=\"modal-content\">
					<pre>" . $_SESSION['view_file'] . "</pre>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['view_file']);
  	} 
?>
<?php
	$temp = new Components('experiment');
	$temp2 = new Components('machine');
	$ex_id = $_GET['ex_id'];
	$ex_name = $_GET['ex_name'];

	$result = $temp->read("*", "id = '$ex_id'");
	while($row = mysqli_fetch_assoc($result)) {
		$pool_id = $row['pool_id'];
		$folder_location = $row['folder_location'];
	}

	$result2 = $temp2->read("*", "pool_id = '$pool_id' AND role = 'Head Node'");
	while($row = mysqli_fetch_assoc($result2)) {
		$ip = $row['ip_address'];
		$root_username = $row['root_username'];
		$root_password = $row['root_password'];
	}
?>

<div id="file_create" class="modal">
	<form action="../controllers/file_create.php" method="post">
		<input type="hidden" name="ex_id" value="<?php echo $ex_id;?>">
		<input type="hidden" name="ex_name" value="<?php echo $ex_name;?>">
		<input type="hidden" name="pool_id" value="<?php echo $pool_id;?>">
		<input type="hidden" name="location" value="<?php echo $folder_location;?>">
		<div class="modal-content">
			<div class="row">
				<div class="input-field col s12">
					<input id="file_name" name="file_name" type="text" class="validate">
					<label for="file_name">File Name</label>
				</div>
			</div>
			<div class="row">
          		<div class="input-field col s12">
            		<textarea id="textarea1" name="file_content" class="materialize-textarea"></textarea>
            		<label for="textarea1">File Content</label>
          		</div>
        	</div>
		</div>
		<div class="modal-footer">
			<div class="center">
				<a class="red darken-1 modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
				<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">input</i>create</button>
			</div>
		</div>
	</form>
</div>

<br>

<br>
<div class="container">
	<nav>
		<div class="nav-wrapper teal">
			<div class="col s12">
				<a href="home.php" class="breadcrumb">Home</a>
				<a href="experiment.php" class="breadcrumb">Experiment</a>
				<a href="files.php?ex_id=<?php echo $_GET['ex_id']; ?>&ex_name=<?php echo $_GET['ex_name'] ?>" class="breadcrumb"><?php echo $_GET['ex_name'] ?> (Files)</a>
			</div>
		</div>
	</nav>
	<br>
	<div class="row">
		<div class="col s6">
			<div class="input-field">
				<input disabled id="search" type="search">
				<label for="search"><i class="material-icons">search</i> Search Files</label>
				<i class="material-icons">close</i>
				<button data-target="file_create" class="btn-large waves-effect waves-light teal lighten-1">Create a File</button>
        	</div>
		</div>
		<div class="col s6">
			<br>
			<form action="../controllers/file_upload.php" enctype="multipart/form-data" method="post">
				<input type="hidden" name="ex_id" value="<?php echo $ex_id;?>">
				<input type="hidden" name="ex_name" value="<?php echo $ex_name;?>">
				<input type="hidden" name="pool_id" value="<?php echo $pool_id;?>">
				<input type="hidden" name="location" value="<?php echo $folder_location;?>">
				<div class="file-field input-field">
      				<div class="btn">
        				<span>Upload Files Here</span>
        				<input type="file" multiple name="ex_files[]">
      				</div>
      				<div class="file-path-wrapper">
        				<input class="file-path validate" type="text" placeholder="Upload files here">
      				</div>
      				<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">input</i>Upload</button>
    			</div>
			</form>
		</div>
	</div>
<?php
	$command = '> content.txt && /bin/chmod 777 content.txt';
	shell_exec($command);

	$command = 'ls -al ' . $folder_location;
	$output = shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" ssh -o StrictHostKeyChecking=no ' . $root_username . '@' . $ip . ' ' . $command);

	$myfile = fopen("content.txt", "w");
	fwrite($myfile, $output);
	fclose($myfile);

	echo "<br>";
	echo "<h4>Files located in folder : " . $folder_location . "</h4>";
	echo "<br>";
	echo "<table class=\"striped\">";
	echo "
		<thead>
        	<tr>
            	<th data-field=\"name\">Name</th>
              	<th data-field=\"permissions\">Permissions</th>
              	<th data-field=\"user\">User</th>
              	<th data-field=\"group\">Group</th>
              	<th data-field=\"size\">Size (in byte)</th>
              	<th data-field=\"time\">Last Modified</th>
              	<th data-field=\"operations\">File Operations</th>
          	</tr>
        </thead>
        <tbody>
	";

	$myfile = fopen("content.txt", "r");
	while(!feof($myfile)) {
		echo "<tr>";
		$line = fgets($myfile);
		$array = preg_split('#\s+#', $line);
		if(count($array) > 3) {
			if($array[8] != "." && $array[8] != "..") {
				echo "<td>" . $array[8] . "</td>";
				echo "<td>" . $array[0] . "</td>";
				echo "<td>" . $array[2] . "</td>";
				echo "<td>" . $array[3] . "</td>";
				echo "<td>" . $array[4] . "</td>";
				echo "<td>" . $array[5] . " " . $array[6] . ", " . $array[7] . "</td>";
				echo "
				<td>
					<a class=\"waves-effect waves-light btn green darken-3\" href=\"../controllers/file_download.php?file=" . $array[8] . "&location=" . $folder_location . "&pool_id=" . $pool_id . "\">Download File</a>
							<a class=\"waves-effect waves-light btn lime darken-3\" href=\"../controllers/file_view.php?file=" . $array[8] . "&location=" . $folder_location . "&pool_id=" . $pool_id . "&ex_id=" . $_GET['ex_id'] . "&ex_name=". $_GET['ex_name'] . "\">View File</a>
							<a class=\"waves-effect waves-light btn red darken-3\" href=\"../controllers/file_delete.php?file=" . $array[8] . "&location=" . $folder_location . "&pool_id=" . $pool_id . "&ex_id=" . $_GET['ex_id'] . "&ex_name=". $_GET['ex_name'] . "\">Delete File</a>
				</td>
				";
			}
		}
		echo "</tr>";
	}
	fclose($myfile);

	echo "
		</tbody>
		</table>
	";

	$command = 'rm content.txt';
	shell_exec($command);
?>

</div>
<br>
<?php
  include 'templates/footer.php';
?>
