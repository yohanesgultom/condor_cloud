<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	}

	if(!(isset($_GET['id']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/argument.php");
	} 
	
  include 'templates/header.php';
  require "../models/Components.php";
?>

<br>
<div class="container">
	<h2>Create New Argument</h2>
	<div class="row">
    <form class="col s12" action="../controllers/argument_new.php" method="post">
		<div class="row">
			<div class="input-field col s12">
				<input value="<?php echo $_GET['id'];?>" id="command_main_id" name="command_main_id" type="text" readonly>
				<label for="command_main_id">Command Main ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="string" name="string" type="text">
				<label for="string">String</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<select class="icons" name="argument_type">
					<option value="" disabled selected>Choose your option</option>
					<option value="INPUT" class="circle">INPUT</option>
					<option value="OUTPUT" class="circle">OUTPUT</option>
					<option value="OTHERS" class="circle">OTHERS</option>
				</select>
				<label>Argument Type</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="description" name="description" type="text">
				<label for="description">Description</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="content_sample" name="content_sample" type="text">
				<label for="content_sample">Content Sample</label>
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">input</i>Create</button>
    </form>
  </div>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
