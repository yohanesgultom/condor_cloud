<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
  } else {
	if($_SESSION['role'] != 'admin') {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
	}
  }

  include 'templates/header.php';
  require "../models/Components.php";
  
   if(isset($_SESSION['error'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal red\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['error'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['error']);
  	} else if(isset($_SESSION['success'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal green lighten-1\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['success'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['success']);
  	}
?>

<br>
<br>
<div class="container">
<?php
	$pool_id = $_GET['id'];
	
	$temp = new Components("machine");
	$result = $temp->read("*", "pool_id = '$pool_id' AND role = 'Head Node'");
	while($row = mysqli_fetch_assoc($result)) {
		$head_username = $row['root_username'];
		$head_password = $row['root_password'];
		$ip = $row['ip_address'];
	}
	
	$command = 'condor_q';
	$output = shell_exec('/usr/bin/sudo -S /usr/bin/sshpass -p "' . $head_password . '" ssh -o StrictHostKeyChecking=no ' . $head_username . '@' . $ip . ' ' . $command);
	
	$command = '> content.txt && /bin/chmod 777 content.txt';
	shell_exec($command);
	
	$myfile = fopen("content.txt", "w");
	fwrite($myfile, $output);
	fclose($myfile);
	
	echo "<br>";
	echo "<h4>Jobs running in this pool</h4>";
	echo "<br>";
	echo "<table class=\"striped\">";
	echo "
		<thead>
        	<tr>
            	<th data-field=\"id\">ID</th>
              	<th data-field=\"owner\">Owner</th>
              	<th data-field=\"submit\">Submitted</th>
              	<th data-field=\"time\">Time Spent</th>
              	<th data-field=\"status\">Status</th>
              	<th data-field=\"priority\">Priority</th>
              	<th data-field=\"size\">Size</th>
				<th data-field=\"command\">Command</th>
          	</tr>
        </thead>
        <tbody>
	";
	
	$myfile = fopen("content.txt", "r");
	while(!feof($myfile)) {
		echo "<tr>";
		$line = fgets($myfile);
		$array = preg_split('#\s+#', $line);
		if(count($array) >= 9 && $array[1] != "ID" && $array[0] == "") {
			echo "<td>" . $array[1] . "</td>";
			echo "<td>" . $array[2] . "</td>";
			echo "<td>" . $array[3] . " " . $array[4] . "</td>";
			echo "<td>" . $array[5] . "</td>";
			echo "<td>" . $array[6] . "</td>";
			echo "<td>" . $array[7] . "</td>";
			echo "<td>" . $array[8] . "</td>";
			echo "<td>" . $array[9] . "</td>";
		} else if(count($array) >= 9 && $array[0] != "") {
			echo "<td>Summary : " . "</td>";
			echo "<td>" . $array[0] . " " . $array[1] . "</td>";
			echo "<td>" . $array[2] . " " . $array[3] . "</td>";
			echo "<td>" . $array[4] . " " . $array[5] . "</td>";
			echo "<td>" . $array[6] . " " . $array[7] . "</td>";
			echo "<td>" . $array[8] . " " . $array[9] . "</td>";
			echo "<td>" . $array[10] . " " . $array[11] . "</td>";
			echo "<td>" . $array[12] . " " . $array[13] . "</td>";
		}
		echo "</tr>";
	}
	fclose($myfile);

	echo "
		</tbody>
		</table>
	";

	$command = 'rm content.txt';
	shell_exec($command);
?>

<br>
<a href="http://<?php echo $_SERVER['SERVER_NAME'];?>/condor_cloud/views/pool_job.php?id=<?php echo $_GET['id'];?>" class="green darken-1 waves-effect waves-green btn-flat">Refresh</a>
<a href="http://<?php echo $_SERVER['SERVER_NAME'];?>/condor_cloud/views/pool.php" class="red darken-1 waves-effect waves-green btn-flat">Back to previous</a>
</div>
<br>
<?php
  include 'templates/footer.php';
?>