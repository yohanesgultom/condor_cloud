<?php
  	session_start();

  	if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} 

  	include 'templates/header.php';
  	require "../models/Components.php";

	if(isset($_SESSION['error'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal red\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['error'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['error']);
  	} else if(isset($_SESSION['success'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal green lighten-1\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['success'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['success']);
  	}
?>

<br>

<br>
<div class="container">
	<nav>
		<div class="nav-wrapper teal">
			<div class="col s12">
				<a href="home.php" class="breadcrumb">Home</a>
				<a href="experiment.php" class="breadcrumb">Experiment</a>
			</div>
		</div>
	</nav>
	<br>
	<div class="row">
		<div class="col s6">
			<div class="input-field">
				<input disabled id="search" type="search">
				<label for="search"><i class="material-icons">search</i> Search your experiment</label>
			<i class="material-icons">close</i>
        </div>
		</div>
		<div class="col s6">
			<br>
			<a href="experiment_new.php" class="waves-effect waves-light btn">Create/Add a new experiment</a>
		</div>
	</div>
	<table class="striped">
	<thead>
		<th>Name</th>
		<th>Description</th>
		<th>Pool ID</th>
		<th>Application ID</th>
		<th>Folder Location</th>
		<th>Operation</th>
	</thead>
		<tbody>	
<?php
	$temp = new Components("experiment");
	$id = $_SESSION['username'];
	$result = $temp->read("*", "user_id = '$id'");
	if(mysqli_num_rows($result) > 0) {
    	while($row = mysqli_fetch_assoc($result)) {
			echo "<tr>";
			$id = $row['id'];
			$name = $row['name'];
			echo "<td>" . $name . "</td>";
			echo "<td>" . $row["description"] . "</td>";
			echo "<td>" . $row["pool_id"] . "</td>";
			echo "<td>" . $row["apps_id"] . "</td>";
			echo "<td>" . $row["folder_location"] . "</td>";
			echo "<td> 
				<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/experiment_edit.php?id=" . $id . "\" class=\"chip\"><i class=\"material-icons tiny\">mode_edit</i>Edit</a>
				<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/temp_experiment_delete.php?id=" . $id . "&name=" . $name . "\" class=\"chip\"><i class=\"material-icons tiny\">delete</i>Delete</a>
				<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/job.php?id=" . $id . "&name=" . $name ."\" class=\"chip\"><i class=\"material-icons tiny\">search</i>See Jobs</a>
				<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/files.php?ex_id=" . $id . "&ex_name=" . $name ."\" class=\"chip\"><i class=\"material-icons tiny\">search</i>See Files</a>
			</td>";
			echo "</tr>";
    	}
	} else {
    	echo "0 results";
	}
?>
		</tbody>
	</table>	
</div>
<br>
<?php
  include 'templates/footer.php';
?>
