<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} 

  if(!(isset($_GET['id']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/command.php");
	}

  include 'templates/header.php';
  require "../models/Components.php";

  $id = $_GET['id'];
  $temp = new Components("experiment");
  $result = $temp->read("*", "id = '$id'");
  if(mysqli_num_rows($result) > 0) {
  	while($row = mysqli_fetch_assoc($result)) {
		$pool_id = $row['pool_id'];
  		$apps_id = $row['apps_id'];
  		$name = $row['name'];
  		$description = $row['description'];
  		$folder_location = $row['folder_location'];
  	}
  }
?>

<br>
<div class="container">
	<h2>Edit An Experiment</h2>
	<div class="row">
    <form class="col s12" action="../controllers/experiment_edit.php" method="post">
		<div class="row">
			<div class="input-field col s12">
				<input disabled value="<?php echo $_SESSION['username'];?>" name="user_id" type="text">
				<label for="user_id">User ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input readonly="readonly" value="<?php echo $id;?>" id="id" name="id" type="text">
				<label for="id">Experiment ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input disabled value="<?php echo $pool_id;?>" name="pool_id" type="text">
				<label for="pool_id">Pool ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input disabled value="<?php echo $apps_id;?>" id="apps_id" name="apps_id" type="text" class="validate">
				<label for="apps_id">Application ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input disabled value="<?php echo $folder_location;?>" name="folder_location" id="folder_location" type="text">
				<label for="folder_location">Folder Location</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input value="<?php echo $name;?>" id="name" name="name" type="text">
				<label for="name">Experiment Name</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<textarea id="textarea1" name="description" class="materialize-textarea" value="<?php echo $description;?>"></textarea>
				<label for="textarea1">Experiment Description</label> 
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">mode_edit</i>Edit</button>
    </form>
  </div>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
