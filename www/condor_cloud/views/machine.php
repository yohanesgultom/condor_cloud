<?php
	session_start();

  	if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	}

	if(!(isset($_GET['id']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/command.php");
	} 

  	include 'templates/header.php';
  	require "../models/Components.php";
	
	if(isset($_SESSION['error'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal red\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['error'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['error']);
  	} else if(isset($_SESSION['success'])) {
		echo "
			<script>
				$(document).ready(function(){
					$('#modalError').modal('open');
				});
        	</script>
			<div id=\"modalError\" class=\"modal green lighten-1\"> 
				<div class=\"modal-content\">
					<h4 style=\"text-align:center;\">" . $_SESSION['success'] . "</h4>	
				</div>
				<div class=\"modal-footer\">
					<a class=\"teal lighten-1 modal-action modal-close waves-effect waves-green btn-flat\">Close</a>
				</div>
			</div>
		";
		unset($_SESSION['success']);
  	}
?>

<br>

<br>
<div class="container">
	<nav>
		<div class="nav-wrapper teal">
			<div class="col s12">
				<a href="home.php" class="breadcrumb">Home</a>
				<a href="pool.php" class="breadcrumb">Pool</a>
				<a href="<?php echo "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/machine.php?id=" . $_GET['id'] . "&name=" . $_GET['name'] ."\""; ?>" class="breadcrumb"><?php echo $_GET['name'];?></a>
			</div>
		</div>
	</nav>
	<br>
	<div class="row">
		<div class="col s6">
			<div class="input-field">
				<input disabled id="search" type="search">
				<label for="search"><i class="material-icons">search</i> Search Machine</label>
			<i class="material-icons">close</i>
        </div>
		</div>
		<div class="col s6">
			<br>
			<a href="<?php echo "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/machine_new.php?id=" . $_GET['id'] . "&name=" . $_GET['name']; ?>" class="waves-effect waves-light btn">Create/Add a New Machine</a>
		</div>
	</div>
	<table class="striped">
	<thead>
		<th>IP Address</th>
		<th>Hostname</th>
		<th>Root Username</th>
		<th>Role</th>
		<th>Description</th>
		<th>Local Path</th>
	</thead>
		<tbody>	
<?php
	$temp = new Components("machine");
	$id = $_GET['id'];
	$result = $temp->read("*", "pool_id = '$id'");
	if(mysqli_num_rows($result) > 0) {
    	while($row = mysqli_fetch_assoc($result)) {
			echo "<tr>";
			$id = $row['id'];
			$hostname = $row["hostname"];
			echo "<td>" . $row["ip_address"] . "</td>";
			echo "<td>" . $hostname . "</td>";
			echo "<td>" . $row["root_username"] . "</td>";
			echo "<td>" . $row["role"] . "</td>";
			echo "<td>" . $row["description"] . "</td>";
			echo "<td>" . $row["local_path"] . "</td>";
			echo "<td> 
				<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/machine_edit.php?id=" . $id . "&po_id=" . $_GET['id'] . "&po_name=" . $_GET['name'] . "\" class=\"chip\"><i class=\"material-icons tiny\">mode_edit</i>Edit</a>
				<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/temp_machine_delete.php?id=" . $id . "&name=" . $hostname . "&po_id=" . $_GET['id'] . "&po_name=" . $_GET['name'] . "\" class=\"chip\"><i class=\"material-icons tiny\">delete</i>Delete</a>
				<a href=\"" . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/controllers/machine_condor.php?id=" . $id . "&name=" . $hostname . "&po_id=" . $_GET['id'] . "&po_name=" . $_GET['name'] . "\" class=\"chip\"><i class=\"material-icons tiny\">hearing</i>Check Condor</a>
			</td>";
			echo "</tr>";
    	}
	} else {
    	echo "0 results";
	}
	
?>
		</tbody>
	</table>	
</div>
<br>
<?php
  include 'templates/footer.php';
?>
