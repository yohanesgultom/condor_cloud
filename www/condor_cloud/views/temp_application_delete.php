<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	}

  include 'templates/header.php';
?>
<div class="container">
	<br>
	<div class="row">
		<h3>Are you sure deleting application <?php echo $_GET['name'];?> ?</h3>
	</div>
	<div class="row">
		<div class="col s6 center">
			<a href="http://<?php echo $_SERVER['SERVER_NAME'];?>/condor_cloud/controllers/application_delete.php?id=<?php echo $_GET['id'];?>&name=<?php echo $_GET['name'];?>" class="waves-effect waves-light btn-large center"><i class="material-icons left">done</i>Yes</a>
		</div>
		<div class="col s6 center">
			<a href="http://<?php echo $_SERVER['SERVER_NAME'];?>/condor_cloud/views/application.php" class="red darken-1 waves-effect waves-green btn-large"><i class="material-icons left">stop</i>No</a>
		</div>
	</div>
</div>
<?php
  include 'templates/footer.php';
?>
