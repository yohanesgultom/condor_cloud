<?php
  session_start();

  if(!(isset($_SESSION['username']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
	} else {
		if($_SESSION['role'] != 'admin') {
			header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
		}
	}

  if(!(isset($_GET['id']))) {
		header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/command.php");
	}

  include 'templates/header.php';
  require "../models/Components.php";

  $id = $_GET['id'];
  $temp = new Components("command_main");
  $result = $temp->read("*", "id = '$id'");
  if(mysqli_num_rows($result) > 0) {
  	while($row = mysqli_fetch_assoc($result)) {
  		$pool_id = $row['pool_id'];
  		$apps_id = $row['apps_id'];
		$string = $row['string'];
		$description = $row['description'];
  	}
	
  }
?>

<br>
<div class="container">
	<h2>Edit A Command</h2>
	<div class="row">
    <form class="col s12" action="../controllers/command_edit.php" method="post">
		<div class="row">
			<div class="input-field col s12">
				<input id="id" value="<?php echo $id;?>" name="id" type="text" readonly>
				<label for="id">Command ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="string" value="<?php echo $string;?>" name="string" type="text">
				<label for="string">Command Name</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12 m6">
				<select class="icons" name="pool_id">
					<?php
						$temp2 = new Components("pool");
						$result2 = $temp2->read("*", "id = '$pool_id'");
						while($row = mysqli_fetch_assoc($result2)) {
							$pool_name = $row['name'];
						}
					?>
					<option value="<?php echo $pool_id;?>" selected><?php echo $pool_name;?></option>
					<?php
						$temp = new Components("pool");
						$result = $temp->read("*", "");
						while($row = mysqli_fetch_assoc($result)) {
							echo "<option value=\"" . $row['id'] . "\" class=\"circle\">" . $row['name'] . "</option>";
						}
					?>
				</select>
				<label>Pool ID</label>
			</div>
			<div class="input-field col s12 m6">
				<select class="icons" name="apps_id">
					<?php
						$temp2 = new Components("application");
						$result2 = $temp2->read("*", "id = '$apps_id'");
						while($row = mysqli_fetch_assoc($result2)) {
							$apps_name = $row['name'];
						}
					?>
					<option value="<?php echo $apps_id;?>" selected><?php echo $apps_name;?></option>
					<?php
						$temp = new Components("application");
						$result = $temp->read("*", "");
						while($row = mysqli_fetch_assoc($result)) {
							echo "<option value=\"" . $row['id'] . "\" class=\"circle\">" . $row['name'] . "</option>";
						}
					?>
				</select>
				<label>Application ID</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<textarea id="textarea1" name="description" class="materialize-textarea"><?php echo $description;?></textarea>
				<label for="textarea1">Command Description</label> 
			</div>
		</div>
		<button type="submit" class="waves-effect waves-light btn center"><i class="material-icons left">mode_edit</i>Edit</button>
    </form>
  </div>
</div>
<br>
<?php
  include 'templates/footer.php';
?>
