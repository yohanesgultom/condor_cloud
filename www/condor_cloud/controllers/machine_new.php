<?php  
	require "../models/Components.php";
	session_start();

	$temp = new Components("machine");

	$pool_id = $_POST['pool_id'];
	$pool_name = $_POST['pool_name'];
	$role = $_POST['role'];
	$ip_address = $_POST['ip_address'];
	$hostname = $_POST['hostname'];
	$root_username = $_POST['root_username'];
	$root_password = $_POST['root_password'];
	$description = $_POST['description'];

	$result = $temp->create("pool_id, ip_address, hostname, root_username, root_password, role, description, local_path", "'$pool_id', '$ip_address', '$hostname', '$root_username', '$root_password', '$role', '$description', ''");
	
	if($result) {
		$id = $temp->get_last_inserted_id();
		$_SESSION['success'] = "Successfully adding new machine " . $hostname . " with ID : " . $id . " in Pool " . $pool_name;
	} else {
		$_SESSION['error'] = "Error adding new machine";
	}

	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/machine.php?id=" . $pool_id . "&name=" . $pool_name);
?>