<?php  
	require "../models/Components.php";
	session_start();

	$temp = new Components("command_main");

	$string = $_POST['string'];
	$pool_id = $_POST['pool_id'];
	$apps_id = $_POST['apps_id'];
	$description = $_POST['description'];

	$result = $temp->create("pool_id, apps_id, string, description", "'$pool_id', '$apps_id', '$string', '$description'");
	if($result) {
		$_SESSION['success'] = "Successfully adding new command : " . $string;
	} else {
		$_SESSION['error'] = "Error adding new command";
	}

	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/command.php");
	
?>