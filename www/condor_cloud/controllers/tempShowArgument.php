<?php 
	require("../models/Components.php");
	$temp = new Components("command_argument");

	$id = $_POST['command_id'];
	$type = $_POST['command_type'];
	$ex_id = $_POST['experiment_id'];

	$result = $temp->read("*", "command_main_id = '$id' AND argument_type = '$type'");
	echo "
		<div class=\"row additional\" >
			<div class=\"input-field col s6\">
    			<select class=\"browser-default\" name=\"argument[]\">
    				<option value=\"\" disabled selected>Choose your argument</option>
    ";
	while($row = mysqli_fetch_assoc($result)) {
    	$string = $row['string'];
      	echo "<option value=\"" . $string . "\">" . $string . "</option>";
	}
    
    echo "
  				</select>
  			</div>
	";

	if($type == "INPUT") {
		echo "<div class=\"input-field col s6\">";
		echo " <select class=\"browser-default\" name=\"files[]\">";
		echo "<option value=\"\" disabled selected>Choose your input files</option>";
		
		$temp2 = new Components('experiment');
		$result2 = $temp2->read("*", "id = '$ex_id'");
		while($row = mysqli_fetch_assoc($result2)) {
			$pool_id = $row["pool_id"];
		}
		
		$temp3 = new Components('machine');
		$result3 = $temp3->read("*", "pool_id = '$pool_id' AND role = 'Head Node'");
		while($row = mysqli_fetch_assoc($result3)) {
			$ip = $row["ip_address"];
			$root_username = $row["root_username"];
			$root_password = $row["root_password"];
			$local_path = $row["local_path"];
		}
		
		$path = $local_path . "/" . $ex_id;
		$command = 'ls -al ' . $path;
		$output = shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" ssh -o StrictHostKeyChecking=no ' . $root_username . '@' . $ip . ' ' . $command);
		
		echo $output;
		$myfile = fopen("content.txt", "w");
		fwrite($myfile, $output);
		fclose($myfile);
		
		$myfile = fopen("content.txt", "r");
		while(!feof($myfile)) {
			$line = fgets($myfile);
			$array = preg_split('#\s+#', $line);
			if(count($array) > 3) {
				if($array[8] != "." && $array[8] != "..") {
					echo "<option value=\"" . $array[8] . "\">" . $array[8] . "</option>";
				}
			}
		}
		fclose($myfile);
		
		$command = 'rm content.txt';
		shell_exec($command);
		
		echo "</select>";
		
	} else if($type == "OUTPUT") {
		echo "
			<div class=\"input-field col s6\">
				<input id=\"files[]\" name=\"files[]\" type=\"text\" class=\"validate\">
          		<label for=\"files[]\">Fill output file name here</label>
		";
	} else if($type == "OTHERS") {
		echo "
			<div class=\"input-field col s6\">
				<input readonly id=\"files[]\" name=\"files[]\" type=\"text\" class=\"validate\" value=\"-\">
		";
	}
	
	echo "
			</div>
		</div>
	";
?>