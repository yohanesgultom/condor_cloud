<?php
	require "../models/Components.php";
	session_start();

	$id = $_GET['id'];
	$ex_id = $_GET['ex_id'];
	$ex_name = $_GET['ex_name'];

	$temp67 = new Components('job');
	$result67 = $temp67->read("*", "id = '$id'");
	while($row = mysqli_fetch_assoc($result67)){
		$execution = $row['execution'];
	}
	
	$temp2 = new Components("experiment");
	$result2 = $temp2->read("*", "id = '$ex_id'");
	while($row = mysqli_fetch_assoc($result2)){
		$pool_id = $row['pool_id'];
	}
	
	$temp3 = new Components("machine");
	$result3 = $temp3->read("*", "pool_id = '$pool_id' AND role = 'Head Node'");
	while($row = mysqli_fetch_assoc($result3)){
		$ip = $row['ip_address'];
		$root_username = $row['root_username'];
		$root_password = $row['root_password'];
		$local_path = $row['local_path'];
	}
	
	$command = 'rm ' . $local_path . '/' . $ex_id . '/data_' . $id . '.log';
	shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" ssh -o StrictHostKeyChecking=no ' . $root_username . '@' . $ip . ' ' . $command);
	
	$command = 'rm ' . $local_path . '/' . $ex_id . '/data_' . $id . '.error';
	shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" ssh -o StrictHostKeyChecking=no ' . $root_username . '@' . $ip . ' ' . $command);
	
	$temp = new Components("job");
	$result = $temp->delete("id = '$id'");
	
	if($result) {
		if($execution == "Remote") {
			$_SESSION["success"] = "Success in deleting job with id : " . $id . " and deleting files : data_" . $id . ".log and data_" . $id . ".error";
		} else {
			$_SESSION["success"] = "Success in deleting job with id : " . $id;
		}
	} else {
		$_SESSION["error"] = "Failure in deleting job";
	}

	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/job.php?id=" . $ex_id . "&name=" . $ex_name);
?>
