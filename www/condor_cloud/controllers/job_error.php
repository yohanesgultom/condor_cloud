<?php
	require "../models/Components.php";
	session_start();

	$job_id = $_GET['job_id'];
	$ex_id = $_GET['ex_id'];
	$ex_name = $_GET['ex_name'];

	$temp = new Components("experiment");
	$result = $temp->read("*", "id = '$ex_id'");
	while($row = mysqli_fetch_assoc($result)) {
		$pool_id = $row['pool_id'];
	}
	
	$temp = new Components("machine");
	$result = $temp->read("*", "pool_id = '$pool_id' AND role = 'Head Node'");
	while($row = mysqli_fetch_assoc($result)) {
		$ip = $row['ip_address'];
		$root_username = $row['root_username'];
		$root_password = $row['root_password'];
		$local_path = $row['local_path'];
	}
	
	$command = 'cat ' . $local_path . "/" . $ex_id . "/" . "data_" . $job_id . ".error";
	$_SESSION['job_log'] = shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" ssh -o StrictHostKeyChecking=no ' . $root_username . '@' . $ip . ' ' . $command);

	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/job.php?id=" . $ex_id . "&name=" . $ex_name);
?>