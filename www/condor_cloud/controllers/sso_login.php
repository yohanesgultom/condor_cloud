<?php
	require "../libraries/sso_login/SSO-master/SSO/SSO.php";
	require "../models/Users.php";

	session_start();
	
	$cas_path = "../libraries/sso_login/phpCAS-master/CAS.php";
    SSO\SSO::setCASPath($cas_path);

    if(SSO\SSO::check()) {
    	$user = SSO\SSO::getUser();
    } else {
    	SSO\SSO::authenticate();
    	$user = SSO\SSO::getUser();
    }

    $temp = new Users("users");
    if(!($temp->isExisted($user->username))) {
		$role = "researcher";
		$name = $user->name;
		$email = $user->username . "@ui.ac.id";
    	$temp->create("username, name, role, email", "'$user->username', '$name', '$role', '$email'");
    }
	
	$id = $user->username;
	$accepted = $temp->read("*", "username = '$id'");
	$_SESSION['username'] = $id;
	while($row = mysqli_fetch_assoc($accepted)) {
		$_SESSION['name'] = $row['name'];
		$_SESSION['role'] = $row['role'];
		$_SESSION['profile_picture'] = $row['profile_picture'];
		$_SESSION['email'] = $row['email'];
	}
    $_SESSION['username'] = $id;

    header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/home.php");
?>
