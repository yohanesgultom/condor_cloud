<?php  
	require "../models/Components.php";

	session_start();

	$temp = new Components("experiment");
	$user_id = $_SESSION['username'];
	$pool_id = $_POST['pool_id'];
	$apps_id = $_POST['apps_id'];
	$name = $_POST['name'];
	$description = $_POST['description'];
	
	$temp2 = new Components("pool");
	$result2 = $temp2->read("folder_path","id = '$pool_id'");
	while($row = mysqli_fetch_assoc($result2)) {
		$path = $row['folder_path'];
    }

	$result = $temp->create("user_id, pool_id, apps_id, name, description", "'$user_id', '$pool_id', '$apps_id', '$name', '$description'");
	if($result) {
		$id = $temp->get_last_inserted_id();
		$new = $path . '/' . $id;
		
		$temp3 = new Components("machine");
		$result3 = $temp3->read("*", "pool_id = '$pool_id' AND role = 'Head Node'");
		while($row = mysqli_fetch_assoc($result3)) {
			$head_username = $row['root_username'];
			$head_password = $row['root_password'];
			$ip = $row['ip_address'];
		}
		
		$command = '/bin/mkdir ' . $new . '; ls -al';
		shell_exec('/usr/bin/sudo -S /usr/bin/sshpass -p "' . $head_password . '" ssh -o StrictHostKeyChecking=no ' . $head_username . '@' . $ip . ' ' . $command);

		$result1 = $temp->update("folder_location = '$new'", "id = '$id'");
		if($result1) {
			$_SESSION['success'] = "Successfully adding new experiment " . $name . " with ID : " . $id . ", and creating folder : " . $new;
		} else {
			$_SESSION['error'] = "Error adding new experiment";
		}

	}
	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/experiment.php");
?>