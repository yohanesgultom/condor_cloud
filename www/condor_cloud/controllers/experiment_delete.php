<?php
	require "../models/Components.php";
	session_start();

	$id = $_GET['id'];
	$name = $_GET['name'];

	
	$temp = new Components("experiment");
	$temp1 = new Components("job");
	$temp3 = new Components("machine");
	
	$result1 = $temp1->read("*", "experiment_id='$id'");
	$status = true;
	
	while($row = mysqli_fetch_assoc($result1)) {
		if($row['status'] == "running" || $row['status'] == "halt" || $row['status'] == "idle") {
			$status = false;
		}
	}
	
	if($status == false) {
		$_SESSION["error"] = "Failure in deleting experiment, there are job that are still running, halt, or idle";
	} else {
		
		$result2 = $temp->read("*", "id = '$id'");
		while($row = mysqli_fetch_assoc($result2)) {
			$pool_id = $row['pool_id'];
			$path = $row['folder_location'];
		}
		
		$result3 = $temp3->read("*", "pool_id = '$pool_id' AND role='Head Node'");
		while($row = mysqli_fetch_assoc($result3)) {
			$username = $row['root_username'];
			$password = $row['root_password'];
			$ip = $row['ip_address'];
		}

		$command = 'rm -rf ' . $path;
		shell_exec('sudo -S /usr/bin/sshpass -p "' . $password . '" ssh -o StrictHostKeyChecking=no ' . $username . '@' . $ip . ' ' . $command);

		$result = $temp->delete("id = '$id'");
		if($result) {
			$_SESSION["success"] = "Success in deleting experiment " . $name . " (with its job) and folder : " . $path;
		} else {
			$_SESSION["error"] = "Failure in deleting experiment";
		}
	}

	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/experiment.php");
?>
