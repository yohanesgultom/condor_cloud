<?php
	require "../models/Components.php";

	$file = $_GET['file'];
	$location = $_GET['location'];
	$pool_id = $_GET['pool_id'];

	$temp = new Components("machine");
	$result = $temp->read("*", "pool_id = '$pool_id' AND role = 'Head Node'");
	while($row = mysqli_fetch_assoc($result)) {
		$ip = $row['ip_address'];
		$root_username = $row['root_username'];
		$root_password = $row['root_password'];
	}

	$command = 'cat ' . $location . "/" . $file;
	$output = shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" ssh -o StrictHostKeyChecking=no ' . $root_username . '@' . $ip . ' ' . $command);

	$command = '> ' . $file . ' && /bin/chmod 777 ' . $file;
	shell_exec($command);

	$myfile = fopen($file, "w");
	fwrite($myfile, $output);
	fclose($myfile);

	$mime = mime_content_type($file);
	header("Content-type: " . $mime);
	header("Content-Disposition:attachment;filename=" . $file);
	readfile($file);

	unlink($file);
?>