<?php
	require "../models/Components.php";
	session_start();

	$id = $_GET['id'];
	$pool_id = $_GET['po_id'];
	$pool_name = $_GET['po_name'];

	$temp = new Components("machine");
	$result = $temp->read("*", "id = '$id'");
	while($row = mysqli_fetch_assoc($result)) {
		$head_username = $row['root_username'];
		$head_password = $row['root_password'];
		$ip = $row['ip_address'];
	}

	$command = 'condor_version';
	$_SESSION['success'] = shell_exec('/usr/bin/sudo -S /usr/bin/sshpass -p "' . $head_password . '" ssh -o StrictHostKeyChecking=no ' . $head_username . '@' . $ip . ' ' . $command);
	
	if($_SESSION['success'] == "") {
		$_SESSION['success'] = "No condor installed";
	} 
	
	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/machine.php?id=" . $pool_id . "&name=" . $pool_name);
?>