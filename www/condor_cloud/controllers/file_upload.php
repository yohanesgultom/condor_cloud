<?php
	require "../models/Components.php";
	session_start();

	$location = $_POST['location'];
	$pool_id = $_POST['pool_id'];
	$ex_id = $_POST['ex_id'];
	$ex_name = $_POST['ex_name'];

	$temp = new Components("machine");
	$result = $temp->read("*", "pool_id = '$pool_id' AND role = 'Head Node'");
	while($row = mysqli_fetch_assoc($result)) {
		$ip = $row['ip_address'];
		$root_username = $row['root_username'];
		$root_password = $row['root_password'];
	}

	$sum = count($_FILES['ex_files']['name']);
	$uploadOk = 1;
	$array = array();

	for($i = 0; $i < $sum; $i++) {
		$temp_path = getcwd() . "/" . $_FILES['ex_files']['name'][$i];
		if(move_uploaded_file($_FILES['ex_files']['tmp_name'][$i], $temp_path)) {
			shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" scp ' . $temp_path . ' ' .  $root_username . '@' . $ip . ':' . $location . '/' . $_FILES['ex_files']['name'][$i]);
			shell_exec('sudo -S /bin/rm ' . $temp_path);
			$array[$i] = $_FILES['ex_files']['name'][$i];
		} else {
			$uploadOk = 0;
			break;
		}
	}
 
 	if($uploadOk == 1) {
		$_SESSION['success'] = "";
		for($j = 0; $j < count($array); $j++) {
			$_SESSION['success'] .= "Successfully Uploading File : " . $array[$j] . "<br>";
		}
		
 	} else {
 		$_SESSION['error'] = "Error Uploading Files";
 	}

 	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/files.php?ex_id=" . $ex_id . "&ex_name=" . $ex_name);
?>