<?php  
	require "../models/Components.php";
	session_start();
	
	$argument = $_POST['argument'];
	$files = $_POST['files'];
	$string = $_POST['command_name'];
	$execution = $_POST['execution'];
	$job_description = $_POST['job_description'];
	
	$pool_id = $_POST['pool_id'];
	$temp = new Components('machine');
	$result = $temp->read("*", "pool_id = '$pool_id' AND role = 'Head Node'");
	while($row = mysqli_fetch_assoc($result)) {
		$ip = $row['ip_address'];
		$root_username = $row['root_username'];
		$root_password = $row['root_password'];
		$local_path = $row['local_path'];		
		$nfs_path = $row['nfs_path'];		
	}
	
	$temp5 = new Components('pool');
	$result5 = $temp5->read("*", "id = '$pool_id'");
	while($row = mysqli_fetch_assoc($result5)) {
		$pool_type = $row['pool_type'];
	}
	
	$ex_id = $_POST['ex_id'];
	$temp2 = new Components('experiment');
	$result2 = $temp2->read("*", "id = '$ex_id'");
	while($row = mysqli_fetch_assoc($result2)) {
		$name = $row['name'];
		$apps_id = $row['apps_id'];
	}
	
	$temp90 = new Components('application');
	$result90 = $temp90->read("*", "id = '$apps_id'");
	while($row = mysqli_fetch_assoc($result90)) {
		$apps_name = $row['name'];
	}
	
	$command = $string;
	$array = array();
	
	if($command == "./") {
		$command .=  $files[0];
		$array[0] = "-," . $files[0];
		$command .=  " " . $argument[1] . " " . $files[0];
		$array[1] = $argument[1] . "," . $files[1];
	} else {
		for($i = 0; $i < count($argument); $i++) {
			if($argument[$i] == "\"\"") {
				$command .= " " . $local_path . "/" . $ex_id . "/" . $files[$i];
				$array[$i] = "-," . $files[$i];
			} else if($files[$i] == "\"\"" || $files[$i] == "-"){
				$command .= " " . $argument[$i];
				$array[$i] = $argument[$i] . ",-";
			} else {
				$command .= " " . $argument[$i] . " " . $local_path . "/" . $ex_id . "/" . $files[$i];
				$array[$i] = $argument[$i] . "," . $files[$i];
			}
		}
	}

	if($execution == "Local") {
		$starting_time =  date("Y-m-d") . " " . date("h:i:s");
		$output = shell_exec('sudo -S sshpass -p "' . $root_password . '" ssh ' . $root_username . '@' . $ip . ' ' . $command);
		$finished_time = date("Y-m-d") . " " . date("h:i:s");
		$temp3 = new Components('job');
		$result3 = $temp3->create("experiment_id, condor_id, string, starting_time, finished_time, description, status, execution", "'$ex_id', '', '$command', '$starting_time', '$finished_time', '$job_description', 'running', '$execution'");
		if($result3) {
			$_SESSION['success'] = "Successfully submitting job : " . $command;
		} else {
			$_SESSION['error'] = "Failed submitting job : " . $command;
		}
	} else {
		$starting_time =  date("Y-m-d") . " " . date("h:i:s");
		
		$temp3 = new Components('job');
		$result3 = $temp3->create("experiment_id, string, starting_time, description, status, execution", "'$ex_id', '$command', '$starting_time', '$job_description', 'running', '$execution'");
		$job_id = $temp3->get_last_inserted_id();
		
		//Create File execute.sh and submit as requirement of condor job
		$command1 = '> execute_' . $job_id . '.sh && /bin/chmod 777 execute_' . $job_id . '.sh && > submit_' . $job_id . ' && /bin/chmod 777 submit_' . $job_id;
		shell_exec($command1);
		
		//Write content of execute.sh file
		if($pool_type == "GPU") {
			$myfile = fopen("execute_" . $job_id . ".sh", "w");
			$txt = "#!/bin/sh\n";
			$txt .= "path=`pwd`" . "\n";
			$txt .= "tar xfz content.tar.gz\n";
			$txt .= "cd " . $ex_id . "\n";
			$txt .= $command . "\n";
			if($command == "/usr/local/gromacs/bin/gmx mdrun -v -deffnm pr") {
				$txt .= "chmod 777 pr.gro" . "\n";
				$txt .= "chmod 777 pr.cpt" . "\n";
				$txt .= "chmod 777 pr.edr" . "\n";
				$txt .= "chmod 777 pr.log" . "\n";
				$txt .= "cp pr.gro \"\$path/pr.gro\"" . "\n";
				$txt .= "cp pr.cpt \"\$path/pr.cpt\"" . "\n";
				$txt .= "cp pr.edr \"\$path/pr.edr\"" . "\n";
				$txt .= "cp pr.log \"\$path/pr.log\"" . "\n";
			} else if($command == "/usr/local/gromacs/bin/gmx mdrun -v -deffnm em") {
				$txt .= "chmod 777 em.gro" . "\n";
				$txt .= "chmod 777 em.tpr" . "\n";
				$txt .= "chmod 777 em.edr" . "\n";
				$txt .= "chmod 777 em.log" . "\n";
				$txt .= "chmod 777 em.trr" . "\n";
				$txt .= "cp em.gro \"\$path/em.gro\"" . "\n";
				$txt .= "cp em.tpr \"\$path/em.tpr\"" . "\n";
				$txt .= "cp em.edr \"\$path/em.edr\"" . "\n";
				$txt .= "cp em.log \"\$path/em.log\"" . "\n";
				$txt .= "cp em.trr \"\$path/em.trr\"" . "\n";
			} else {
				$txt .= "cp * \"\$path/\"" . "\n";
			}
			fwrite($myfile, $txt);
			fclose($myfile);
		} else if($pool_type == "Cluster") {
			//if($apps_name == "Gromacs") {
				$myfile = fopen("execute_" . $job_id . ".sh", "w");
			
				$txt = "#!/bin/sh\n";
				$txt .= "/bin/chmod 777 content.tar.gz\n";
				$txt .= "/bin/cp content.tar.gz {$nfs_path}/content.tar.gz\n";
				$txt .= "cd {$nfs_path}\n";
				$txt .= "/bin/tar xfz content.tar.gz\n";
				$txt .= "/bin/chmod -R 777 {$ex_id}*\n";
				$txt .= "cd {$ex_id}\n";
				$txt .= "{$command}";
				
				fwrite($myfile, $txt);
				fclose($myfile);
			//}
		}
		
		//Write content of submit file
		$myfile = fopen("submit_" . $job_id, "w");
		$txt = "universe = vanilla\n";
		$txt .= "executable = ./execute_" . $job_id . ".sh\n";
		$txt .= "error = data_" . $job_id . ".error\n";
		$txt .= "log = data_" . $job_id . ".log\n";
		$txt .= "should_transfer_files = yes\n";
		// if($pool_type == "Cluster") {
		// 	$txt .= "requirements = (machine == \"komputer2\")\n";
		// }
		$txt .= "when_to_transfer_output = on_exit\n";
		$txt .= "transfer_input_files = content.tar.gz,execute_" . $job_id . ".sh\n";
		
		$exist = 0;
		for($i = 0; $i < count($array); $i++) {
			$temp = explode(" ", $array[$i]);
			if($temp[0] == "OUTPUT") {
				$exist = 1;
				if($i == 0) {
					if($i == (count($array) - 1)) {
						$text = "transfer_output_files = " . $temp[1] ."\nqueue";
					} else {
						$text = "transfer_output_files = " . $temp[1] . ",";
					}
				} else if($i == (count($array) - 1)) {
					$text .= $temp[1] . "\nqueue";
				} else {
					$text .= $temp[1] . ",";
				}
			}
		}

		if($exist == 0) {
			if($command == "/usr/local/gromacs/bin/gmx mdrun -v -deffnm pr") {
				$txt .= "transfer_output_files = pr.edr,pr.cpt,pr.log,pr.gro\nqueue";
			} else if($command == "/usr/local/gromacs/bin/gmx mdrun -v -deffnm em"){
				$txt .= "transfer_output_files = em.edr,em.trr,em.tpr,em.gro,em.log\nqueue";
			} else if (stripos($command, "-o mdout.1GPU -inf mdinfo.1GPU -x mdcrd.1GPU -r restrt.1GPU")) {
				$txt .= "transfer_output_files = mdout.1GPU,mdinfo.1GPU,mdcrd.1GPU,restrt.1GPU\nqueue";
			} else {
				$txt .= "#transfer_output_files = output.txt\nqueue";
			}
		} else {
			$txt .= $text;
		}
		
		fwrite($myfile, $txt);
		fclose($myfile);
		
		//Tar All Files Needed for the Job
		$command1 = '> temp.sh && /bin/chmod 777 temp.sh';
		shell_exec($command1);
		$myfile = fopen("temp.sh", "w");
		$txt = "#!/bin/sh\n";
		$txt .= "cd " . $local_path . "\n";
		$txt .= "tar -czvf " . $local_path . "/" . $ex_id . "/content.tar.gz " . $ex_id;
		fwrite($myfile, $txt);
		fclose($myfile);
		shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" scp temp.sh ' . $root_username . '@' . $ip . ':~/temp.sh');
		shell_exec('rm temp.sh');
		shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" ssh -o StrictHostKeyChecking=no ' . $root_username . '@' . $ip . ' ./temp.sh');
		shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" ssh -o StrictHostKeyChecking=no ' . $root_username . '@' . $ip . ' rm temp.sh');

		//Transfer Files submit and tes.sh to directory of the experiment of job
		shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" scp submit_' . $job_id . ' ' . $root_username . '@' . $ip . ':' . $local_path . '/' . $ex_id . '/submit_' . $job_id);
		shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" scp execute_' . $job_id . '.sh ' . $root_username . '@' . $ip . ':' . $local_path . '/' . $ex_id . '/execute_' . $job_id . '.sh');
		
		//Remove Both Files from Current Working Directory
		shell_exec('rm submit_' . $job_id);
		shell_exec('rm execute_' . $job_id . '.sh');
		
		//Submit Job
		$command1 = '> temp.sh && /bin/chmod 777 temp.sh';
		shell_exec($command1);
		$myfile = fopen("temp.sh", "w");
		$txt = "#!/bin/sh\n";
		$txt .= "cd " . $local_path . "/" . $ex_id . "\n";
		$txt .= "condor_submit submit_" . $job_id;
		fwrite($myfile, $txt);
		fclose($myfile);
		shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" scp temp.sh ' . $root_username . '@' . $ip . ':~/temp.sh');
		shell_exec('rm temp.sh');
		$output = shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" ssh -o StrictHostKeyChecking=no ' . $root_username . '@' . $ip . ' ./temp.sh');
		shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" ssh -o StrictHostKeyChecking=no ' . $root_username . '@' . $ip . ' rm temp.sh');
		
		//Retrieve Condor ID
		$temp1 = explode(" ", $output);
		$temp45 = explode(".", $temp1[6]);
		$condor_id = $temp45[0];
		$result45 = $temp3->update("condor_id = '$condor_id'", "id = '$job_id'");
		
		if($result45) {
			$_SESSION['success'] = "Successfully submitting job : " . $command . " with Condor ID : " . $condor_id;
		} else {
			$_SESSION['error'] = "Failed submitting job : " . $command;
		}
	}
	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/job.php?id=" . $ex_id . "&name=" . $name);
?>