<?php  
	require "../models/Components.php";
	session_start();

	$temp = new Components("pool");
	$temp1 = new Components("machine");

	$pool_name = $_POST['pool_name'];
	$pool_type = $_POST['pool_type'];
	$pool_description = $_POST['pool_description'];
	
	$head_role = $_POST['role'];
	$hostname = $_POST['hostname'];
	$ip = $_POST['ip'];
	$head_username = $_POST['username'];
	$head_password = $_POST['password'];
	$head_description = $_POST['head_description'];
	
	$command = 'hostname';
	$output = exec('sudo -S /usr/bin/sshpass -p "' . $head_password . '" ssh -o StrictHostKeyChecking=no ' . $head_username . '@' . $ip . ' ' . $command);
	
	if($output != $hostname) {
		$_SESSION['error'] = "Failed in Connecting through SSH";
	} else {
		$command = '/bin/mkdir /home/' . $head_username . '/experiment';
		shell_exec('sudo -S /usr/bin/sshpass -p "' . $head_password . '" ssh -o StrictHostKeyChecking=no ' . $head_username . '@' . $ip . ' ' . $command);
		$path = "/home/" . $head_username . "/experiment";
		$result = $temp->create("name, pool_type, description, headNode_ip, folder_path", "'$pool_name', '$pool_type', '$pool_description', '$ip', '$path'");
		if($result) {
			$id = $temp->get_last_inserted_id();
			// $path = "/home/" . $head_username . "/experiment";
			$result1 = $temp1->create("pool_id, ip_address, hostname, root_username, root_password, role, description, local_path", "'$id', '$ip', '$hostname', '$head_username', '$head_password', '$head_role', '$head_description', '$path'");
			// $result2 = $temp->update("folder_path = '$path'", "id = '$id'");
			if($result1) {
				$_SESSION['success'] = "Experiment directory in " . $path . ", success in adding pool : " . $pool_name . ", and head node machine " . $hostname;
			} else {
				$_SESSION['error'] = "Failed in Creating Pool";
			}
		} else {
			$_SESSION['error'] = "Failed in Creating Pool";
		}
	}
	
	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/pool.php");
?>