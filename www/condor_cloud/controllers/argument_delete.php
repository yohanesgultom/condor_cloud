<?php
	require "../models/Components.php";
	session_start();

	$id = $_GET['id'];
	
	$temp = new Components("command_argument");
	$result = $temp->delete("id = '$id'");
	
	if($result) {
		$_SESSION["success"] = "Success in deleting argument with ID : " . $id;
	} else {
		$_SESSION["error"] = "Failure in deleting argument";
	}

	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/argument.php?id=" . $_GET['co_id'] . "&name=" . $_GET['co_name']);
?>
