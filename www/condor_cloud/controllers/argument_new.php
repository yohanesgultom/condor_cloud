<?php  
	require "../models/Components.php";
	session_start();

	$temp = new Components("command_argument");
	$temp2 = new Components("command_main");

	$command_main_id = $_POST['command_main_id'];
	$string = $_POST['string'];
	$argument_type = $_POST['argument_type'];
	$description = $_POST['description'];
	$content_sample = $_POST['content_sample'];

	$result = $temp->create("command_main_id, string, argument_type, description, content_sample", "'$command_main_id', '$string', '$argument_type', '$description', '$content_sample'");
	if($result) {
		$_SESSION['success'] = "Successfully adding new argument : " . $string;
	} else {
		$_SESSION['error'] = "Error adding new argument";
	}

	$result2 = $temp2->read("*", "id = '$command_main_id'");
	while($row = mysqli_fetch_assoc($result2)) {
		$id = $row['id'];
		$name = $row['string'];
	}
	
	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/argument.php?id=" . $id . "&name=" . $name);
?>