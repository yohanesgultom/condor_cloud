<?php
	require "../models/Components.php";
	session_start();

	$id = $_GET['id'];
	
	$temp = new Components("command_main");
	$result = $temp->delete("id = '$id'");
	
	if($result) {
		$_SESSION["success"] = "Success in deleting command with ID : " . $id;
	} else {
		$_SESSION["error"] = "Failure in deleting command";
	}

	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/command.php");
?>
