<?php  
	require "../models/Components.php";
	session_start();

	$temp = new Components("application");

	$name = $_POST['name'];
	$description = $_POST['description'];

	$result = $temp->create("name, description", "'$name', '$description'");
	if($result) {
		$id = $temp->get_last_inserted_id();
		$_SESSION['success'] = "Successfully adding new application " . $name . " with ID : " . $id;
	} else {
		$_SESSION['error'] = "Error adding new application";
	}

	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/application.php");
?>