<?php
	require "../libraries/sso_login/SSO-master/SSO/SSO.php";
	require "../models/Users.php";
	$cas_path = "../libraries/sso_login/phpCAS-master/CAS.php";
    SSO\SSO::setCASPath($cas_path);
    
	session_start();
	session_unset();
	session_destroy();
	SSO\SSO::logout();
	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/index.php");
?>