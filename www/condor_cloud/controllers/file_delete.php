<?php
	require "../models/Components.php";
	session_start();

	$file = $_GET['file'];
	$location = $_GET['location'];
	$pool_id = $_GET['pool_id'];
	$ex_id = $_GET['ex_id'];
	$ex_name = $_GET['ex_name'];

	$temp = new Components("machine");
	$result = $temp->read("*", "pool_id = '$pool_id' AND role = 'Head Node'");
	while($row = mysqli_fetch_assoc($result)) {
		$ip = $row['ip_address'];
		$root_username = $row['root_username'];
		$root_password = $row['root_password'];
	}

	$command = 'rm ' . $location . "/" . $file;
	shell_exec('sudo -S /usr/bin/sshpass -p "' . $root_password . '" ssh -o StrictHostKeyChecking=no ' . $root_username . '@' . $ip . ' ' . $command);

	$_SESSION['success'] = "Successfully Deleting File : " . $file;

	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/views/files.php?ex_id=" . $ex_id . "&ex_name=" . $ex_name);
?>