<?php
	$root_username = $_POST['root_username'];
	$root_pass = $_POST['root_pass'];
	$root_ip = $_POST['root_ip'];
	
	$portal_username = $_POST['portal_username'];
	$portal_pass = $_POST['portal_pass'];
	$portal_path = $_POST['portal_file'];
	$portal_ip = $_POST['portal_ip'];
	
	$headNode_username = $_POST['headNode_username'];
	$headNode_pass = $_POST['headNode_pass'];
	$headNode_path = $_POST['headNode_file'];
	$headNode_ip = $_POST['headNode_ip'];
	
	$hpc_username = $_POST['hpc_username'];
	$hpc_pass = $_POST['hpc_pass'];
	$hpc_path = $_POST['hpc_file'];
	$hpc_ip = $_POST['hpc_ip'];
	
	$temp = explode('/', $portal_path);
	for($i = 0; $i < count($temp); $i++) {
		if($i == (count($temp) - 1)) {
			$portal_file = $temp[$i];
		}
	}
	
	$temp = explode('/', $headNode_path);
	for($i = 0; $i < count($temp); $i++) {
		if($i == (count($temp) - 1)) {
			$headNode_file = $temp[$i];
		}
	}
	
	$temp = explode('/', $hpc_path);
	for($i = 0; $i < count($temp); $i++) {
		if($i == (count($temp) - 1)) {
			$hpc_file = $temp[$i];
		}
	}
	
	//Retrieve File from Portal
	$command = 'cat ' . $portal_path;
	$portal = shell_exec('/usr/bin/sudo -S /usr/bin/sshpass -p "' . $root_pass . '" ssh -o StrictHostKeyChecking=no ' . $portal_username . '@' . $portal_ip . ' ' . $command);
	$command = '> portal_' . $portal_file . ' && /bin/chmod 777 portal_' . $portal_file;
	shell_exec($command);
	file_put_contents('portal_' . $portal_file, $portal);
	
	//Retrieve File from Head Node
	$command = 'cat ' . $headNode_path;
	$headNode = shell_exec('/usr/bin/sudo -S /usr/bin/sshpass -p "' . $root_pass . '" ssh -o StrictHostKeyChecking=no ' . $headNode_username . '@' . $headNode_ip . ' ' . $command);
	$command = '> headNode_' . $headNode_file . ' && /bin/chmod 777 headNode_' . $headNode_file;
	shell_exec($command);
	file_put_contents('headNode_' . $headNode_file, $headNode);
	
	//Retrieve File from HPC Resource
	$command = 'cat ' . $hpc_path;
	$hpc = shell_exec('/usr/bin/sudo -S /usr/bin/sshpass -p "' . $root_pass . '" ssh -o StrictHostKeyChecking=no ' . $hpc_username . '@' . $hpc_ip . ' ' . $command);
	$command = '> hpc_' . $hpc_file . ' && /bin/chmod 777 hpc_' . $hpc_file;
	shell_exec($command);
	file_put_contents('hpc_' . $hpc_file, $hpc);
	
	$portal_file_sha =  sha1_file('portal_' . $portal_file) . "<br>";
	$headNode_file_sha = sha1_file('headNode_' . $headNode_file) . "<br>";
	$hpc_file_sha = sha1_file('hpc_' . $hpc_file) . "<br>";
	
	$portal_size = filesize('portal_' . $portal_file);
	$headNode_size = filesize('headNode_' . $headNode_file);
	$hpc_size = filesize('hpc_' . $hpc_file);
	
	session_start();
	
	if(($portal_file_sha == $headNode_file_sha) && ($portal_file_sha == $hpc_file_sha) && ($headNode_file_sha == $hpc_file_sha)) {
		$_SESSION['content_check'] = "<div class=\"card-panel green lighten-2\">Content of files are similar</div>";
	} else {
		$_SESSION['content_check'] = "<div class=\"card-panel red lighten-2\">Content of files are not similar</div>";
	}
	
	if(($portal_size == $headNode_size) && ($portal_size == $hpc_size) && ($headNode_size == $hpc_size)) {
		$_SESSION['size_check'] = "<div class=\"card-panel green lighten-2\">Size of files are similar</div>";
	} else {
		$_SESSION['size_check'] = "<div class=\"card-panel red lighten-2\">Size of files are not similar</div>";
	}
	
	$_SESSION['portal_path'] = $portal_path;
	$_SESSION['portal_file'] = $portal_file;
	$_SESSION['portal_file_sha'] = $portal_file_sha;
	$_SESSION['portal_size'] = $portal_size;
	
	$_SESSION['headNode_path'] = $headNode_path;
	$_SESSION['headNode_file'] = $headNode_file;
	$_SESSION['headNode_file_sha'] = $headNode_file_sha;
	$_SESSION['headNode_size'] = $headNode_size;
	
	$_SESSION['hpc_path'] = $hpc_path;
	$_SESSION['hpc_file'] = $hpc_file;
	$_SESSION['hpc_file_sha'] = $hpc_file_sha;
	$_SESSION['hpc_size'] = $hpc_size;
	
	shell_exec('rm portal_' . $portal_file);
	shell_exec('rm headNode_' . $headNode_file);
	shell_exec('rm hpc_' . $hpc_file);
	
	header("Location: " . "http://" . $_SERVER['SERVER_NAME']. "/condor_cloud/file_integrity_result.php");
?>


	