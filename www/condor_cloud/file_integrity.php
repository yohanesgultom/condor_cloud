<?php
  header("Expires: Wed, 31 Dec 2025 11:59:59 GMT");
  header("Cache-Control: no-cache");
  header("Pragma: no-cache");
?>

<!DOCTYPE html>
	<head>
		<title>Condor Cloud - File Integrity</title>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
		<meta name="author" content="Dennis Pratama Kamah, 1306464562, Fakultas Ilmu Komputer, Universitas Indonesia"/>
		<link href="libraries/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
		<script src="libraries/js/jquery-3.1.1.min.js" type="text/javascript"></script>
		<script src="libraries/js/materialize.min.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="container">
			<form action="controllers/file_integrity.php" method="post">
				<div class="row">
					<div class="input-field col s6">
						<input id="root_username" name="root_username" type="text" class="validate">
						<label for="root_username">Root Username</label>
					</div>
					<div class="input-field col s6">
						<input id="root_pass" name="root_pass" type="password" class="validate">
						<label for="root_pass">Root Password</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="root_ip" name="root_ip" type="text" class="validate">
						<label for="root_ip">Root IP Address</label>
					</div>
				</div>
				<h5>File in Portal<h5>
				<div class="row">
					<div class="input-field col s6">
						<input id="portal_username" name="portal_username" type="text" class="validate">
						<label for="portal_username">Portal Username</label>
					</div>
					<div class="input-field col s6">
						<input id="portal_pass" name="portal_pass" type="password" class="validate">
						<label for="portal_pass">Portal Password</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s6">
						<input id="portal_ip" name="portal_ip" type="text" class="validate">
						<label for="portal_ip">Portal IP Address</label>
					</div>
					<div class="input-field col s6">
						<input id="portal_file" name="portal_file" type="text" class="validate">
						<label for="portal_file">Absolute Path of Portal File</label>
					</div>
				</div>
				<h5>File in Head Node<h5>
				<div class="row">
					<div class="input-field col s6">
						<input id="headNode_username" name="headNode_username" type="text" class="validate">
						<label for="headNode_username">Head Node Username</label>
					</div>
					<div class="input-field col s6">
						<input id="headNode_pass" name="headNode_pass" type="password" class="validate">
						<label for="headNode_pass">Head Node Password</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s6">
						<input id="headNode_ip" name="headNode_ip" type="text" class="validate">
						<label for="headNode_ip">Head Node IP Address</label>
					</div>
					<div class="input-field col s6">
						<input id="headNode_file" name="headNode_file" type="text" class="validate">
						<label for="headNode_file">Absolute Path of Head Node File</label>
					</div>
				</div>
				<h5>File in HPC Resource<h5>
				<div class="row">
					<div class="input-field col s6">
						<input id="hpc_username" name="hpc_username" type="text" class="validate">
						<label for="hpc_username">HPC Username</label>
					</div>
					<div class="input-field col s6">
						<input id="hpc_pass" name="hpc_pass" type="password" class="validate">
						<label for="hpc_pass">HPC Password</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s6">
						<input id="hpc_ip" name="hpc_ip" type="text" class="validate">
						<label for="hpc_ip">HPC Resource IP Address</label>
					</div>
					<div class="input-field col s6">
						<input id="hpc_file" name="hpc_file" type="text" class="validate">
						<label for="hpc_file">Absolute Path of HPC File</label>
					</div>
				</div>
				<input class="btn waves-effect waves-light" type="submit" name="submit" value="Check File Integrity">  
			</form>
			<br><br><br>
		</div>
	</body>
</html>


	