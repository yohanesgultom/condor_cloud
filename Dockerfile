ARG PHP_VERSION
FROM nanoninja/php-fpm:$PHP_VERSION
RUN apt-get update && apt-get install -y sudo sshpass
RUN adduser user && \
    echo "user ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/user && \
    chmod 0440 /etc/sudoers.d/user
USER user